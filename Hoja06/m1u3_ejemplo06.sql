/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 06
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

USE ejemplo6;

DELIMITER //

-- 01 Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te calcule el total autom�ticamente. 
--   El total debe ser unidades por precio. 
--   Recordar que para el insert solo pod�is utilizar NEW. Adem�s, deb�is recordar que para actualizar los valores de una 
--   tabla en la inserci�n debe ser BEFORE (con el AFTER pod�is actualizar otra tabla, pero no la misma). 

CREATE OR REPLACE TRIGGER ventasBI BEFORE INSERT ON ventas FOR EACH ROW 
  BEGIN
    SET new.total = new.precio * new.unidades;
  END//


-- 02 Crear un disparador para la tabla ventas para que cuando inserte un registro me sume el total a la tabla productos (en el campo cantidad).
CREATE OR REPLACE TRIGGER ventasAI AFTER INSERT ON ventas FOR EACH ROW 
  BEGIN
    UPDATE productos SET cantidad = cantidad + new.unidades
      WHERE producto = new.producto;
  END//


-- 03 Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo te calcule el total autom�ticamente. 
CREATE OR REPLACE TRIGGER ventasBU BEFORE UPDATE ON ventas FOR EACH ROW 
  BEGIN
    SET new.total = new.precio * new.unidades;
  END // 

-- 04 Crear un disparador para la tabla ventas para que cuando actualice un registro me sume el total a la tabla productos (en el campo cantidad). 
CREATE OR REPLACE TRIGGER ventasAI BEFORE UPDATE ON ventas FOR EACH ROW 
  BEGIN
    UPDATE productos p
      SET p.cantidad = p.cantidad + new.unidades - old.unidades
    WHERE p.producto = new.producto;
  END//

-- 05 Crear un disparador para la tabla productos que si cambia el c�digo del producto te sume todos los totales de ese producto de la tabla ventas 
CREATE OR REPLACE TRIGGER productosBU BEFORE UPDATE ON ventas FOR EACH ROW 
  BEGIN
    IF NEW.producto != OLD.producto THEN
      SET new.unidades = (SELECT SUM(ventas.unidades) FROM ventas WHERE ventas.producto = NEW.producto);
    END IF;
  END //

-- 06 Crear un disparador para la tabla productos que si eliminas un producto te elimine todos los productos del mismo c�digo en la tabla ventas 
CREATE OR REPLACE TRIGGER productosAD AFTER DELETE ON productos FOR EACH ROW
  BEGIN
    DELETE FROM ventas WHERE producto = OLD.producto; 
  END //

-- 07 Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del campo cantidad de la tabla productos (en el campo cantidad). 
CREATE OR REPLACE TRIGGER ventasAD AFTER DELETE ON ventas FOR EACH ROW
  BEGIN
    UPDATE productos
        SET cantidad = cantidad - old.unidades
      WHERE producto = old.producto;
  END //

-- 08 Modificar el disparador 3 para que modifique la tabla productos actualizando el valor del campo cantidad en funcion del total. 
CREATE OR REPLACE TRIGGER ventasBU BEFORE UPDATE ON ventas FOR EACH ROW 
  BEGIN
    SET new.total = new.precio * new.unidades;
    UPDATE productos
        SET cantidad = cantidad + new.unidades - old.unidades
      WHERE producto = new.producto;

  END // 

DELIMITER ; 