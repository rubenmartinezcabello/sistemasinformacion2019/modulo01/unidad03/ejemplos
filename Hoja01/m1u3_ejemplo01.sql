/*****************************************************************************
*
*   M�dulo 1 - Unidad 3 - Ejemplo 01
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

CREATE DATABASE IF NOT EXISTS programacion 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;

USE programacion;

DELIMITER //


CREATE OR REPLACE PROCEDURE ej101_if(IN n1 int, IN n2 int)
  -- CONTAINS SQL
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa instrucciones de control de flujo IF...THEN)'
  BEGIN
    DECLARE s int DEFAULT 0; 
    SET s = n1;
    IF (n2>s) 
      THEN 
        SET s = n2;
    END IF;
    SELECT s;
  END//

CREATE OR REPLACE PROCEDURE ej101_totales (IN n1 int, IN n2 int)
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa funciones de totales de tabla)'
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE t_ej101_totales (n int);
    INSERT IGNORE INTO t_ej101_totales (n) VALUE (n1);
    INSERT IGNORE INTO t_ej101_totales (n) VALUE (n2);
    SELECT MAX(n) FROM t_ej101_totales;
    DROP TABLE t_ej101_totales;
  END//

CREATE OR REPLACE PROCEDURE ej101_func(IN n1 int, IN n2 int)
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa funcion GREATEST())'
  BEGIN
    SELECT GREATEST(n1, n2);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej102_tablas (m int, n int, o int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE mayorDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO mayorDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MAX(num) FROM mayorDe3_tablas;
    DROP TABLE mayorDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE ej102_progr (m int, n int, o int)
  BEGIN
    IF (n > m) 
      THEN 
        IF (o > n)
          THEN SELECT o;
          ELSE SELECT n;
        END IF;
      ELSE 
        IF (o > m)
          THEN SELECT o;
          ELSE SELECT m;
        END IF;
    END IF;
  END//
CREATE OR REPLACE PROCEDURE ej102_progr (m int, n int, o int)
  BEGIN
    DECLARE mayor int;
    SET mayor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    SELECT mayor;
  END//

CREATE OR REPLACE PROCEDURE ej102_funct (m int, n int, o int)
  BEGIN
    SELECT GREATEST(m, n, o);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej103_tablas (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE minmaxDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO minmaxDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MIN(num) INTO min FROM minmaxDe3_tablas;
    SELECT MAX(num) INTO max FROM minmaxDe3_tablas;
    DROP TABLE minmaxDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE ej103_progr (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    DECLARE mayor int;
    DECLARE menor int;
    SET mayor = m;
    SET menor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (n<menor) 
      THEN SET menor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    IF (o<menor) 
      THEN SET menor = o;
    END if;
    SET min = menor;
    SET max = mayor;
  END//

CREATE OR REPLACE PROCEDURE ej103_funct (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    SET min = LEAST(m, n, o);
    SET max = GREATEST(m, n, o);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej104_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT DATEDIFF(future, past);
  END//

CREATE OR REPLACE PROCEDURE ej104_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT TO_DAYS(future) - TO_DAYS(past);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej105_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT PERIOD_DIFF( EXTRACT(YEAR_MONTH FROM future), EXTRACT(YEAR_MONTH FROM past) ) ;
  END//

CREATE OR REPLACE PROCEDURE ej105_tsdiff (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT TIMESTAMPDIFF(MONTH, past, future);
  END//

CREATE OR REPLACE PROCEDURE ej105_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT MONTH(future) - MONTH(past) + 12*(YEAR(future)-YEAR(past));
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej106_funct(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE y int;
    DECLARE m int;
    DECLARE d int;
    SET y = YEAR(future) - YEAR(past);
    SET m = MONTH(future) - MONTH(past);
    SET d = DAY(future) - DAY(past);
    IF (m<0) 
      THEN BEGIN
          SET m = m+12; 
          SET y = y-1;
        END;
    END IF;
    IF (d<0) 
      THEN BEGIN
          SET d = d + DAY(LAST_DAY(past)); 
          SET m = m-1;
        END;
    END IF;

    SET days = d;
    SET months = m;
    SET years = y;
  END//

CREATE OR REPLACE PROCEDURE ej106_tsdiff(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SET years = TIMESTAMPDIFF(YEAR, past, future);
    SET months = TIMESTAMPDIFF(MONTH, past, future);
    SET days = TIMESTAMPDIFF(DAY, past, future);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej107(IN s varchar(255))
  BEGIN
    SELECT CHAR_LENGTH(s);
  END//

DELIMITER ; 



-- Pruebas de ejercicio 1
CALL ej101_if(1,3);
CALL ej101_totales(1,3);
CALL ej101_func(1,3);

CALL ej101_totales(0, 0);
CALL ej101_if (0, 0);
CALL ej101_func (0, 0);

CALL ej101_totales(-17, -3);
CALL ej101_if (-17, -3);
CALL ej101_func (-17, -3);

CALL ej102_tablas(0, 0, 0);
CALL ej102_progr (0, 0, 0);
CALL ej102_funct (0, 0, 0);

CALL ej102_tablas(3, 2, 4);
CALL ej102_progr (3, 2, 4);
CALL ej102_funct (3, 2, 4);



SET @minimo = null;
set @maximo = null;

CALL ej103_tablas(0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(-4, 2, 7, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (7, 2, -4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (7, -4, 2, @minimo, @maximo);
SELECT @minimo, @maximo;

SET @minimo = null;
SET @maximo = null;



CALL ej104_funct('1978-04-15', '1981-02-23');
CALL ej104_subst('1978-04-15', '1981-02-23');

CALL ej105_funct('1978-04-15', '1981-02-23');
CALL ej105_tsdiff('1978-04-15', '1981-02-23');
CALL ej105_subst('1978-04-15', '1981-02-23');



SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ej106_funct('1978-04-15', '1981-02-23', @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;

SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ej106_tsdiff('1978-04-15', '1981-02-23',  @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;


CALL ej107('1234');
CALL ej107('12345678');
CALL ej107('12345678901234567890');
