/**
 * M�dulo 1 - Bases de datos
 * Unidad 3 - Vistas y programaci�n
 *
 * -- Procedimientos almacenados
 *
 * Ejemplos: Hoja 9
 *
 * Por Rub�n Mart�nez Cabello <runar.orested@gmail.com>
**/

DROP FUNCTION buscarEmpleado;
DROP FUNCTION buscarEmpresa;
DROP PROCEDURE crearRelacionEstan;
DROP PROCEDURE crearTablaTrabajan;
DROP PROCEDURE crearTablaEmpleados;
DROP PROCEDURE crearTablaEmpresa;
DROP PROCEDURE crearTablaPoblacion;
DROP DATABASE ejemplo9;



CREATE DATABASE IF NOT EXISTS ejemplo9
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;

USE ejemplo9;


/*
SHOW TABLES;
DROP TABLE IF EXISTS trabajan;
DROP TABLE IF EXISTS empleados;
DROP TABLE IF EXISTS empresa;
DROP TABLE IF EXISTS poblacion;
*/




-- 1.a Creacion de tablas por script MySQL
/*
CREATE OR REPLACE TABLE empleados(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  fechaNacimiento date,
  correo varchar(20),
  edad int,
  PRIMARY KEY (id)
) ENGINE=MYISAM;
CREATE OR REPLACE TABLE empresa(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  direccion varchar(20),
  numeroEmpleados int DEFAULT 0,
  PRIMARY KEY (id)
) ENGINE=MYISAM;
CREATE OR REPLACE TABLE trabajan(
  id int AUTO_INCREMENT,
  empleado int,
  empresa int,
  fechaInicio date,
  fechaFin date,
  baja bool DEFAULT FALSE,
  PRIMARY KEY(id),
  CONSTRAINT trabajanUK UNIQUE KEY (empleado,empresa)
) ENGINE=MYISAM;
*/



-- 1.b Creacion de tablas por procedimientos almacenados
DELIMITER //
-- Empleados
CREATE OR REPLACE PROCEDURE crearTablaEmpleados(test int)
  BEGIN
    CREATE OR REPLACE TABLE empleados(
      id int AUTO_INCREMENT,
      nombre varchar(20),
      fechaNacimiento date,
      correo varchar(20),
      edad int,
      PRIMARY KEY (id)
    ) ENGINE=MYISAM;
  
    IF(test=1) THEN
      INSERT INTO empleados (nombre, fechaNacimiento)
        VALUES 
        ('empleado1','1980/1/1'),
        ('empleado2','1980/1/2'),
        ('empleado3','1980/1/3'),
        ('empleado4','1980/1/4'),
        ('empleado5','1980/1/5');
    END IF;
  END //

-- Empresa
CREATE OR REPLACE PROCEDURE crearTablaEmpresa (test int)
  BEGIN
    CREATE OR REPLACE TABLE empresa(
      id int AUTO_INCREMENT,
      nombre varchar(20),
      direccion varchar(20),
      numeroEmpleados int DEFAULT 0,
      PRIMARY KEY (id)
    ) ENGINE=MYISAM;
  
    IF(test=1) THEN
      INSERT INTO empresa (nombre, direccion) VALUES 
        ('empresa1','direccion1'),
        ('empresa2','direccion2'),
        ('empresa3','direccion3'),
        ('empresa4','direccion4'),
        ('empresa5','direccion5');
    END IF ;
  END //

-- Poblacion
CREATE OR REPLACE PROCEDURE crearTablaPoblacion(test int)
  BEGIN
    CREATE OR REPLACE TABLE poblacion(
      codigo int AUTO_INCREMENT,
      nombre varchar(20),
      numTrabajadores int DEFAULT 0,
      numEmpresas int DEFAULT 0,
      PRIMARY KEY (codigo)
    ) ENGINE=MYISAM;
  
    IF(test=1) THEN
      INSERT INTO poblacion (codigo, nombre)
        VALUES 
        (39001, "Santander"),
        (24001, "Le�n");
    END IF;
  END //

-- 
CREATE OR REPLACE PROCEDURE crearTablaTrabajan(test int)
  BEGIN
    CREATE OR REPLACE TABLE trabajan(
      id int AUTO_INCREMENT,
      empleado int,
      empresa int,
      fechaInicio date,
      fechaFin date,
      baja bool DEFAULT FALSE,
      PRIMARY KEY(id),
      CONSTRAINT trabajanUK UNIQUE KEY (empleado,empresa)
      ) ENGINE=MYISAM;
  
    IF (test=1) THEN 
      INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin) VALUES 
        (1,1,'2015/1/1',NULL);
    END IF;
  END //

-- 
CREATE OR REPLACE PROCEDURE crearRelacionEstan()
  BEGIN
    -- ALTER TABLE IF EXISTS empresa ADD COLUMN IF NOT EXISTS estaEnPoblacion int DEFAULT NULL;
    ALTER TABLE empresa ADD COLUMN estaEnPoblacion int DEFAULT 0;
  END //

DELIMITER ;






-- Crear tablas
CALL crearTablaEmpleados(0);
CALL crearTablaEmpresa(0);
CALL crearTablaPoblacion(1);
CALL crearTablaTrabajan(0);
CALL crearRelacionEstan();







-- Una vez creadas las tablas, podemos crear funciones Y TRIGGERS que las usan
DELIMITER //


-- crear una funcion que reciba como argumento un id de empresa
-- y me devuelva si la empresa existe
CREATE OR REPLACE FUNCTION buscarEmpresa(codigo int) RETURNS int 
  BEGIN
    DECLARE valor int DEFAULT 0;
    SELECT COUNT(*) INTO valor FROM empresa WHERE id=codigo;
    RETURN valor;
  END //

-- crear una funcion que reciba como argumento un id de empleado
-- y me devuelva si el empleado existe
CREATE OR REPLACE FUNCTION buscarEmpleado(codigo int) RETURNS int 
  BEGIN
    DECLARE valor int DEFAULT 0;
    SELECT COUNT(*) INTO valor FROM empleados WHERE id=codigo;
    RETURN valor;
  END //

-- en el disparador de trabaja que ya tenia 
-- voy a intentar utilizar las funciones creadas
-- crear el disparador para trabaja
CREATE OR REPLACE TRIGGER trabajanBI BEFORE INSERT ON trabajan FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio no es v�lida';
    END IF;
    -- el empleado debe estar en la tabla empleados
    SELECT COUNT(*) INTO numero FROM empleados WHERE id=new.empleado;
    IF (numero = 0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'el trabajador no est� registrado';
    END IF;
    -- la empresa debe existir
    SELECT COUNT(*) INTO numero FROM empresa WHERE id=new.empresa;
    IF (numero = 0) THEN
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;
    --  si la fecha final no esta vacia baja pasa a true
    IF (new.fechaFin IS NOT NULL) THEN
      SET new.baja = TRUE;
    END IF;
  END //
  
-- realizar los disparadores para actualizar
CREATE OR REPLACE TRIGGER trabajanBU BEFORE UPDATE ON trabajan FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;

    -- la fecha de inicio debe ser menor o igual que la fecha de fin
    IF(new.fechaInicio>new.fechaFin) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio no es v�lida';
    END IF;

   -- el empleado debe estar en la tabla empleados
    SELECT COUNT(*) INTO numero FROM empleados WHERE id=new.empleado;
    IF (numero = 0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'el trabajador no est� registrado';
    END IF;

    -- la empresa debe existir
    SELECT COUNT(*) INTO numero FROM empresa WHERE id=new.empresa;
    IF (numero = 0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'esa empresa no existe';
    END IF;

    --  si la fecha final no esta vacia baja pasa a true
    IF (new.fechaFin IS NOT NULL) THEN
      SET new.baja = TRUE;
    ELSE 
      SET new.baja=FALSE;
    END IF;

  END //
  
DELIMITER ;









-- Ejercicio 2
/*
INSERT INTO empleados (nombre, fechaNacimiento) VALUES 
  ('empleado1','1980/1/1'),
  ('empleado2','1980/1/2'),
  ('empleado3','1980/1/3'),
  ('empleado4','1980/1/4'),
  ('empleado5','1980/1/5')
;
*/


-- Ejercicio 3
DELIMITER //

CREATE OR REPLACE PROCEDURE recalcularEmpleados()
  BEGIN
    UPDATE empleados e
      SET e.correo = CONCAT(e.nombre, "@prueba.es"), e.edad = TIMESTAMPDIFF( year, e.fechaNacimiento, NOW() )
    WHERE true;
  END //

DELIMITER ;

-- CALL recalcularEmpleados();





-- Ejercicio 4
DELIMITER //

CREATE OR REPLACE TRIGGER empleadosBI BEFORE INSERT ON empleados FOR EACH ROW
  BEGIN
    SET new.correo = CONCAT(new.nombre, "@prueba.es");
    SET new.edad = TIMESTAMPDIFF( year, new.fechaNacimiento, NOW() ); 
  END //

CREATE OR REPLACE TRIGGER empleadosBU BEFORE UPDATE ON empleados FOR EACH ROW
  BEGIN
    SET new.correo = CONCAT(new.nombre, "@prueba.es");
    SET new.edad = TIMESTAMPDIFF( year, new.fechaNacimiento, NOW() ); 
  END //

DELIMITER ;



-- 5
/*
INSERT INTO empresa (nombre, direccion, estaEnPoblacion) VALUES 
  ('empresa1','direccion1', 24001),
  ('empresa2','direccion2', 39001),
  ('empresa3','direccion3', 24001),
  ('empresa4','direccion4', 39001),
  ('empresa5','direccion5', 24001);
*/

-- 6
TRUNCATE TABLE trabajan;
DELETE FROM trabajan WHERE TRUE;
/*
INSERT HIGH_PRIORITY INTO trabajan (empleado, empresa, fechaInicio, fechaFin) VALUES 
  (1,1,"1981-1-1","1985-12-31"),
  (2,2,"1982-1-1",null),
  (3,3,"1983-1-1",null),
  (4,4,"1984-1-1",null),
  (5,5,"1985-1-1",null),
  (1,3,"1986-1-1",null);
*/

/*
SELECT empleados.nombre AS nombreEmpleado, empleados.fechaNacimiento, empresa.nombre AS nombreEmpresa, trabajan.fechaInicio, trabajan.fechaFin, poblacion.nombre AS nombrePoblacion
  FROM
    trabajan
    INNER JOIN empleados ON empleados.id = trabajan.empleado
    INNER JOIN empresa ON empresa.id = trabajan.empresa
    LEFT OUTER JOIN poblacion ON empresa.estaEnPoblacion = poblacion.codigo
;
*/

-- 7 - Realizar con disparadores la actualizaci�n de todos los campos calculados
DELIMITER //

-- numero empleados
CREATE OR REPLACE TRIGGER trabajanAI AFTER INSERT ON trabajan FOR EACH ROW
  BEGIN
    IF new.baja = FALSE THEN 
      UPDATE empresa e 
          SET e.numeroEmpleados = e.numeroEmpleados + 1 
        WHERE e.id = new.empresa;
    END IF;
  END//

CREATE OR REPLACE TRIGGER trabajanAU AFTER UPDATE ON trabajan FOR EACH ROW
  BEGIN
    IF old.baja = FALSE THEN 
      UPDATE empresa e 
          SET e.numeroEmpleados = e.numeroEmpleados - 1 
        WHERE e.id = old.empresa;
    END IF;
    IF new.baja = FALSE THEN 
      UPDATE empresa e 
          SET e.numeroEmpleados = e.numeroEmpleados + 1 
        WHERE e.id = new.empresa;
    END IF;
  END//

CREATE OR REPLACE TRIGGER trabajanAD AFTER DELETE ON trabajan FOR EACH ROW
  BEGIN
    IF old.baja = FALSE THEN 
      UPDATE empresa e 
          SET e.numeroEmpleados = e.numeroEmpleados - 1 
        WHERE e.id = old.empresa;
    END IF;
  END//


-- numero de empresas
CREATE OR REPLACE TRIGGER empresaAI AFTER INSERT ON empresa FOR EACH ROW
  BEGIN
    UPDATE poblacion SET poblacion.numEmpresas = poblacion.numEmpresas+1 WHERE poblacion.codigo = new.estaEnPoblacion;
  END //

CREATE OR REPLACE TRIGGER empresaAU AFTER UPDATE ON empresa FOR EACH ROW
  BEGIN
    UPDATE poblacion 
        SET poblacion.numEmpresas = poblacion.numEmpresas-1, 
            poblacion.numTrabajadores = poblacion.numTrabajadores - old.numeroEmpleados
      WHERE poblacion.codigo = old.estaEnPoblacion;

    UPDATE poblacion 
        SET poblacion.numEmpresas = poblacion.numEmpresas+1, 
            poblacion.numTrabajadores = poblacion.numTrabajadores + new.numeroEmpleados
      WHERE poblacion.codigo = new.estaEnPoblacion;
  END //

CREATE OR REPLACE TRIGGER empresaAD AFTER DELETE ON empresa FOR EACH ROW
  BEGIN
    UPDATE poblacion 
        SET poblacion.numEmpresas = poblacion.numEmpresas-1,
            poblacion.numTrabajadores = poblacion.numTrabajadores - old.numeroEmpleados
      WHERE poblacion.codigo = old.estaEnPoblacion;
  END //

DELIMITER ;


-- Pruebas de los triggers

INSERT INTO empleados (nombre, fechaNacimiento) VALUES 
  ('empleado1','1980/1/1'),
  ('empleado2','1980/1/2'),
  ('empleado3','1980/1/3'),
  ('empleado4','1980/1/4'),
  ('empleado5','1980/1/5')
;
INSERT INTO empresa (nombre, direccion, estaEnPoblacion) VALUES 
  ('empresa1','direccion1', 24001),
  ('empresa2','direccion2', 39001),
  ('empresa3','direccion3', 24001),
  ('empresa4','direccion4', 39001),
  ('empresa5','direccion5', 24001)
;

INSERT HIGH_PRIORITY INTO trabajan (empleado, empresa, fechaInicio, fechaFin) VALUES 
  (1,1,"1981-1-1","1985-12-31"),
  (2,2,"1982-1-1",null),
  (3,3,"1983-1-1",null),
  (4,4,"1984-1-1",null),
  (5,5,"1985-1-1",null),
  (1,3,"1986-1-1",null);


SELECT * FROM empresa;
SELECT * FROM poblacion;
INSERT INTO empresa ( nombre, direccion, estaEnPoblacion) VALUES ('Pescanueva', 'Calle del puerto, 3', 24001);
SELECT * FROM empresa;
SELECT * FROM poblacion;
UPDATE empresa SET estaEnPoblacion = 39001 WHERE nombre = 'Pescanueva';
SELECT * FROM empresa;
SELECT * FROM poblacion;
DELETE FROM empresa WHERE nombre = 'Pescanueva';
SELECT * FROM empresa;
SELECT * FROM poblacion;







/*
SELECT empleados.nombre AS nombreEmpleado, empleados.fechaNacimiento, empresa.nombre AS nombreEmpresa, trabajan.fechaInicio, trabajan.fechaFin, poblacion.nombre AS nombrePoblacion
  FROM
    trabajan
    INNER JOIN empleados ON empleados.id = trabajan.empleado
    INNER JOIN empresa ON empresa.id = trabajan.empresa
    LEFT OUTER JOIN poblacion ON empresa.estaEnPoblacion = poblacion.codigo
;
*/

/*
SELECT t.empresa AS idEmpresa, count(empleado) AS numEmpl FROM trabajan t GROUP BY t.empresa;
UPDATE empresa INNER JOIN (SELECT t.empresa AS idEmpresa, count(empleado) AS numEmpl FROM trabajan t GROUP BY t.empresa) empleados ON empresa.id = empleados.idEmpresa
  SET empresa.numeroEmpleados = empleados.numEmpl
  WHERE TRUE ;

DELIMITER //

CREATE OR REPLACE PROCEDURE recalcularEmpresa()
  BEGIN
    UPDATE empresa INNER JOIN (SELECT t.empresa AS idEmpresa, count(empleado) AS numEmpl FROM trabajan t GROUP BY t.empresa) empleados ON empresa.id = empleados.idEmpresa
      SET empresa.numeroEmpleados = empleados.numEmpl
      WHERE TRUE ;
  END//

DELIMITER ;


CALL recalcularEmpresa();
  */