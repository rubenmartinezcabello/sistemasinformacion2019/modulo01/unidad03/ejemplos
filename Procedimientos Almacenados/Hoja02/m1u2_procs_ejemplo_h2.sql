/**
 * M�dulo 1 - Bases de datos
 * Unidad 3 - Vistas y programaci�n
 *
 * -- Procedimientos almacenados
 *
 * Ejemplos: Hoja 1, Ejercicio 1
 *
 * Por Rub�n Mart�nez Cabello <runar.orested@gmail.com>
**/

CREATE DATABASE IF NOT EXISTS m1u3_ejemplo2 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;


USE m1u3_ejemplo2;



DELIMITER //

CREATE OR REPLACE PROCEDURE buscaletra (m int, n int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE mayorDe2_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO mayorDe2_tablas (num) VALUE (m);
    INSERT IGNORE INTO mayorDe2_tablas (num) VALUE (n);
    SELECT MAX(num) FROM mayorDe2_tablas;
    DROP TABLE mayorDe2_tablas;
  END//

CREATE OR REPLACE PROCEDURE mayorDe2_progr (m int, n int)
  BEGIN
    IF (n > m) 
      THEN SELECT n;
      ELSE SELECT m;
    END IF;
  END//

CREATE OR REPLACE PROCEDURE mayorDe2_funct (m int, n int)
  BEGIN
    SELECT GREATEST(m, n);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

DELIMITER ; 







-- Pruebas de ejercicio 1
/*
CALL mayorDe2_tablas(0, 0);
CALL mayorDe2_progr (0, 0);
CALL mayorDe2_funct (0, 0);

CALL mayorDe2_tablas(3, 2);
CALL mayorDe2_progr (3, 2);
CALL mayorDe2_funct (3, 2);

CALL mayorDe2_tablas(-17, -3);
CALL mayorDe2_progr (-17, -3);
CALL mayorDe2_funct (-17, -3);

CALL mayorDe2_tablas(-4, 2);
CALL mayorDe2_progr (2, -4);
CALL mayorDe2_funct (2, -4);
*/
/*
CALL mayorDe3_tablas(0, 0, 0);
CALL mayorDe3_progr (0, 0, 0);
CALL mayorDe3_funct (0, 0, 0);

CALL mayorDe3_tablas(3, 2, 4);
CALL mayorDe3_progr (3, 2, 4);
CALL mayorDe3_funct (3, 2, 4);

CALL mayorDe3_tablas(-17, -3, 1);
CALL mayorDe3_progr (-17, -3, 1);
CALL mayorDe3_funct (-17, -3, 1);

CALL mayorDe3_tablas(-4, 2, 7);
CALL mayorDe3_progr (7, 2, -4);
CALL mayorDe3_funct (7, -4, 2);
*/

/*
SET @minimo = null;
set @maximo = null;

CALL minmaxDe3_tablas(0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(-4, 2, 7, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (7, 2, -4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (7, -4, 2, @minimo, @maximo);
SELECT @minimo, @maximo;

SET @minimo = null;
SET @maximo = null;
*/

/*
CALL daydiff_funct('1978-04-15', '1981-02-23');
CALL daydiff_subst('1978-04-15', '1981-02-23');
*/
/*
CALL monthdiff_funct('1978-04-15', '1981-02-23');
CALL monthdiff_tsdiff('1978-04-15', '1981-02-23');
CALL monthdiff_subst('1978-04-15', '1981-02-23');
*/
/*
SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ymddiff_funct('1978-04-15', '1981-02-23', @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;

SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ymddiff_tsdiff('1978-04-15', '1981-02-23',  @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;
*/

CALL charcount('1234');
CALL charcount('12345678');
CALL charcount('12345678901234567890');

