-- CREATE DATABASE programacion CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE programacion;


-- Modulo 1 - Unidad 1 - Diseño de BBDD.
-- Modulo 1 - Unidad 2 - Clausulas query - Select, insert, update, delete.
-- Modulo 1 - Unidad 3 - TSQL - Programación para procedimientos almacenados.
-- Modulo 2 - Unidad 1 - Módulo CMS (Wordpress, etc).
-- Modulo 2 - Unidad 2 - Git y programación colaborativa.
-- Modulo 2 - Unidad 3 - Aplicacion CMS en Yii, empleando un ORM como abstracción


/** M1U3 - Transact SQL, lenguage de programación de SQL (TSQL)
  - SET
  - IF
  - WHILE

  -- Procedimiento (Procedure) - Conjunto de instrucciones que se ejecutan como una sola.
  -- Funciones -
  -- Eventos - 
  -- Disparadores - 

  Difeencias entre mostrar (imprimir) y devolver

  Llamadas a los procedimientos almacenados
    Con la instrucción CALL:
    CALL currentDate();


    -- Declarar variables
    DECLARE <nombre_variable> <tipo> [DEFAULT <valorXdefecto>];

    -- Asignar variables
      -- SELECT nombre INTO %v; Copiar a variable
      -- SET variable = <operacion>;
**/



  -- CONTROL DE FLUJO
/* 
  
-- IF THEN ELSE END IF

  Hay 2 formas de usarse, como operador o como instrucción

  COMO OPERADOR: 
    Similar al operador ? : en C/C++

  COMO INSTRUCCION:
    Similar al formato empleado en 

    IF (condicion) THEN
      bloque_true;
    ELSE
      bloque_false;
    END IF;

    IF (condicion1) THEN
      bloque_c1;
    ELSEIF (condicion2) THEN
      bloque_c2;
    ELSEIF (condicion3) THEN
      bloque_c3;
    ELSE
      bloque_otros;
    END IF;
  
-- CASE
CASE value
  WHEN search_condition THEN statement_list;
  WHEN search_condition THEN statement_list;
  WHEN search_condition THEN statement_list;
 END CASE;

*/



  
-- Fibonacci.


CREATE OR REPLACE 
  PROCEDURE Fibonacci(numero int)
    BEGIN
    
    END
  ;



CALL Fibonacci(0);
/*
DROP PROCEDURE p5;
DROP PROCEDURE p6;
SHOW ALL TABLES;
  */



DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p4 (minuendo int, sustraendo int)
    BEGIN
      DECLARE resultado int DEFAULT 0;
      SET resultado = minuendo - sustraendo;
      SELECT resultado;
    END //

DELIMITER ;



/*

USE ciclistas;

DELIMITER //
CREATE OR REPLACE PROCEDURE cociente (dividendo int, divisor int)
  BEGIN
    IF divisor <> 0 THEN 
      SELECT dividendo/divisor;
    ELSE
      SELECT "infinito";
    END IF;
  END //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE PROCEDURE faltan()
  BEGIN
    DECLARE i int DEFAULT 0;
    DECLARE r int DEFAULT NULL;
    SELECT MAX(numetapa) INTO i FROM etapa;
    SET i=i+1;
    WHILE i > 0 DO
      SELECT numetapa INTO r FROM etapa WHERE numetapa = i;
      IF r <> NULL THEN
        SELECT "Falta: ", i;
      END IF;
      SET i = i - 1;
    END WHILE;
  END //
DELIMITER ;

-- CALL cociente(1,0);
CALL faltan();

-- SELECT MAX(numetapa)  FROM etapa;
  */