-- CREATE DATABASE programacion CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE programacion;




/**
  6.1 Crea el procedimiento P6 que calcule la suma y el producto de 2 variables enteras
      Crea una tabla con la suma y el producto de las variables, y campo clave dinamico
**/

DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p6(IN n1 int, IN n2 int)
    BEGIN
      -- Declarar variable
      DECLARE suma int DEFAULT 0;
      DECLARE producto int DEFAULT 0;

      -- Crear la tabla si no existe, si no la deja igual
      CREATE TABLE IF NOT EXISTS sumaproducto (
        id int AUTO_INCREMENT,
        n1 int DEFAULT 0,
        n2 int DEFAULT 0,
        suma int DEFAULT 0,
        producto int DEFAULT 0,
        PRIMARY KEY (id)
      );
      -- Declarar salida
      SET suma = n1 + n2;
      SET producto = n1 * n2;
      -- 
      INSERT INTO sumaproducto (n1, n2, suma, producto) VALUE (n1, n2, suma, producto);
    END //

DELIMITER ;

/**
  6.2 Probar
**/
CALL p6(1,0);
CALL p6(1,1);
CALL p6(2,1);
CALL p6(3,2);
SELECT * FROM sumaproducto s;




/**
  7.1 Crea el procedimiento p7 que exponencia (n elevado a m) un n�mero, y que almacena del resultado en la tabla ej33
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p7(IN base int, IN exponente int)
    BEGIN
      -- Declarar variable
      DECLARE potencia int DEFAULT 0;

      -- Crear la tabla si no existe, si no la deja igual
      CREATE TABLE IF NOT EXISTS ej33 (
        base int DEFAULT 0,
        exponente int DEFAULT 0,
        potencia int DEFAULT 0,
        PRIMARY KEY (base, exponente)
      );
      -- Declarar salida
      SET potencia = POWER(base, exponente);
      -- 
      INSERT INTO ej33 (base, exponente, potencia) VALUE (base, exponente, potencia)
        ON DUPLICATE KEY UPDATE potencia = potencia;
    END //

DELIMITER ;


/**
  7.2 Pruebas
**/
CALL p7(1,0);
CALL p7(1,1);
CALL p7(1,2);
CALL p7(1,3);
CALL p7(2,0);
CALL p7(2,1);
CALL p7(2,2);
CALL p7(2,3);
CALL p7(3,0);
CALL p7(3,1);
CALL p7(3,2);
CALL p7(3,3);
CALL p7(3,3);
SELECT * FROM ej33;



/**
  8.1 Crea el procedimiento p8 que calcula la raiz cuadrada de un n�mero (n). Almacena el n�mero y su raiz en la tabla raiz
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p8(IN radicando int)
    BEGIN

      -- Declarar variable
      DECLARE raiz float DEFAULT 0;

      -- Crear la tabla si no existe, si no la deja igual
      CREATE TABLE IF NOT EXISTS raiz_cuadrada (
        radicando int DEFAULT 0,
        raiz float DEFAULT 0,
        PRIMARY KEY (radicando)
      );

      -- Declarar salida
      SET raiz = SQRT(radicando);

      -- 
      INSERT INTO raiz_cuadrada (radicando, raiz) VALUE (radicando, raiz) 
        ON DUPLICATE KEY UPDATE raiz = raiz;
    END //

DELIMITER ;

/**
  8.2 Crea el procedimiento p8 que calcula la raiz cuadrada de un n�mero (n). Almacena el n�mero y su raiz en la tabla raiz
**/
CALL p8(2);
CALL p8(4);
CALL p8(9);
SELECT * FROM raiz_cuadrada rc;





/**
  9.1 Crea p9, un procedimiento que almacena una tabla de argumentos de hasta 50 caracteres y la longitud de cada argumento
**/
DELIMITER //

DROP PROCEDURE IF EXISTS p9 //
CREATE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p9 (IN argumento varchar(50))
    BEGIN
      CREATE TABLE IF NOT EXISTS texto (
        id int AUTO_INCREMENT,
        texto varchar(50),
        PRIMARY KEY (id)
      );
  
      INSERT INTO texto (texto) VALUE (argumento);
    END//


DROP PROCEDURE IF EXISTS p9 //
CREATE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p9 (IN argumento varchar(50))
    BEGIN
      CREATE TABLE IF NOT EXISTS texto (
        id int AUTO_INCREMENT,
        texto varchar(50),
        longitud int,
        PRIMARY KEY (id)
      );
      INSERT INTO texto (texto, longitud) VALUE (argumento, CHAR_LENGTH(argumento));
    END//

DELIMITER ;

/**
  9.2 Prueba
**/
CALL p9('argumento uno');
CALL p9('-- arg=2');
CALL p9('3rd hollywood choice for a blockbuster');
CALL p9('I�t�rn�ti�n�liz�ti�n ');
SELECT * FROM texto;


/**
 10.1 Crea P10, procedimiento el cual pasa un n�mero e indica si es mayor que 100
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p10 (IN numero int)
    BEGIN
      IF (numero > 100) THEN
        SELECT CONCAT(numero, " es mayor");
      END IF;
    END//

DELIMITER ;

/**
 10.2 Crea P10, procedimiento el cual pasa un n�mero e indica si es mayor que 100
**/
CALL p10(99);
CALL p10(100);
CALL p10(101);
