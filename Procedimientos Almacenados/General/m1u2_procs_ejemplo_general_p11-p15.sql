-- CREATE DATABASE programacion CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE programacion;




/**
 11.1 Crea p11, procedimiento el cual pasa un n�mero e indica si es mayor, menor o igual que 100
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p11 (IN numero int)
    BEGIN
      IF (numero = 100) THEN
        SELECT CONCAT(numero, " es igual");
      ELSEIF (numero > 100) THEN
        SELECT CONCAT(numero, " es mayor");
      ELSE
        SELECT CONCAT(numero, " es menor");
      END IF;
    END//

DELIMITER ;

/**
 11.2 Probar
**/
/*
CALL p11(99);
CALL p11(100);
CALL p11(101);
*/



/**
 12.1 Crear un procedimiento que le pasas una nota numerica y te devuelve los siguientes valores:
       nota < 5 Suspenso
  5 <= nota < 7 Aprobado
  7 <= nota < 9 Notable
  9 <= nota     Sobresaliente
  Hazlo con IF
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p12 (IN nota float)
    BEGIN
      DECLARE evaluacion varchar(13) DEFAULT "No evaluado";
      IF (nota>=5) AND (nota<7) 
        THEN SET evaluacion = "Aprobado";
      ELSEIF (nota>=7) AND (nota<9) 
        THEN SET evaluacion = "Notable";
      ELSEIF (nota>=9) 
        THEN SET evaluacion = "Sobresaliente";
        ELSE SET evaluacion = "Suspenso";
      END IF;
      SELECT CONCAT(evaluacion,' (', nota, ')' ) AS nota;
    END //

DELIMITER ; 

/**
 12.2 probar
**/
/*
CALL p12(-3);
CALL p12(3.8);
CALL p12(5.2);
CALL p12(6.9);
CALL p12(8.8);
CALL p12(9.1);
CALL p12(10);
CALL p12(11);
*/



/**
 13.1  Crear un procedimiento p13 que le pasas una nota num�rica y te devuelve los siguientes valores:
       nota < 5 Suspenso
  5 <= nota < 7 Aprobado
  7 <= nota < 9 Notable
  9 <= nota     Sobresaliente
  Hazlo empleando CASE y una variable temporal
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p13 (IN nota float)
    BEGIN
      DECLARE evaluacion varchar(13) DEFAULT "No evaluado";
      CASE    
        WHEN nota >= 9 THEN SET evaluacion = "Sobresaliente";
        WHEN nota >= 7 THEN SET evaluacion = "Notable";
        WHEN nota >= 5 THEN SET evaluacion = "Aprobado";
        ELSE SET evaluacion = "Suspenso";
      END CASE;
      SELECT CONCAT(evaluacion,' (', nota, ')' );
    END //

DELIMITER ; 


/**
 13.2 Probar
**/
/*
CALL p13(-3);
CALL p13(3.8);
CALL p13(5.2);
CALL p13(6.9);
CALL p13(8.8);
CALL p13(9.1);
CALL p13(10);
CALL p13(11);
*/



/**
 14.1 Crea el procedimiento p14 que calcule la suma de 2 numeros. La suma la almacena en un argumento de salida
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p14 (IN a float, IN b float, OUT r float)
    BEGIN
      SET r = a + b;
    END //

DELIMITER ; 

/**
 14.2 Probar
**/
SET @resultado = NULL;
CALL p14(23, 77, @resultado);
SELECT @resultado;




/**
 15.1 Crea el procedimiento p15 que recibe como argumentos
    IN int n1: 1er operando
    IN int n2: 2do operando
    OUT int res: resultado
    IN varchar(20) calculo: Si calculo = '+' o 'suma', hace la suma, si '*' o 'producto', el producto
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p15 (IN n1 float, IN n2 float, OUT res float, IN calculo varchar(20))
    BEGIN
      CASE (calculo)
        WHEN '+' THEN SET res = n1 + n2;
        WHEN 'suma' THEN set res = n1 + n2;
        WHEN '-' THEN SET res = n1 - n2;
        WHEN 'resta' THEN SET res = n1 + n2;
        WHEN '*' THEN SET res = n1 * n2;
        WHEN 'producto' THEN SET res = n1 * n2;
        WHEN '/' THEN SET res = n1 / n2;
        WHEN 'division' THEN SET res = n1 / n2;
        WHEN '%' THEN SET res = n1 % n2;
        WHEN 'mod' THEN SET res = n1 % n2;
        WHEN 'modulo' THEN SET res = n1 % n2;
        WHEN 'div' THEN SET res = n1 DIV n2;
        ELSE SET res = NULL;
      END CASE;
    END//
DELIMITER ;

/**
 15.2 Prueba todas las operaciones posibles
**/

SET @resultado = 0;
CALL p15(3, 2, @resultado, 'suma');
SELECT @resultado;
CALL p15(3, 2, @resultado, '-');
SELECT @resultado;
CALL p15(3, 2, @resultado, '*');
SELECT @resultado;
CALL p15(3, 2, @resultado, '/');
SELECT @resultado;
CALL p15(3, 2, @resultado, 'div');
SELECT @resultado;
CALL p15(3, 2, @resultado, '%');
SELECT @resultado;
CALL p15(3, 2, @resultado, 'exp');
SELECT @resultado;

-- UNSET @resultado; -- No funciona con MySQL
