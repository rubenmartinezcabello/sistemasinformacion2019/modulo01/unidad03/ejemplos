-- CREATE DATABASE programacion CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE programacion;




/**
 16.1 Procedimiento p16 que muestra a los numeros de 1 al 10, con un bucle while
**/
DELIMITER //

CREATE OR REPLACE 
  PROCEDURE p16()
    BEGIN
      DECLARE contador int DEFAULT 0;
      WHILE contador < 10 DO 
        SET contador = contador +1;
        SELECT contador;
      END WHILE;
    END//

DELIMITER ;
/**
 16.2 Probar
**/
/*
CALL p16();
*/



/**
 17.1 Procedimiento p17 que muestra a los numeros de 1 al 10, con un bucle while, en una tabla temporal, para que salgan una sola pesta�a de respuesta.
**/
DELIMITER //

CREATE OR REPLACE 
  PROCEDURE p17()
    BEGIN
      DECLARE contador int DEFAULT 0;
      CREATE OR REPLACE TEMPORARY TABLE p17 (
        id int AUTO_INCREMENT,
        n int DEFAULT null,
        PRIMARY KEY (id)
      );
      WHILE contador < 10 DO 
        SET contador = contador +1;
        INSERT INTO p17 (n) VALUE (contador);
      END WHILE;
      SELECT n FROM p17;
      DROP TABLE p17;
    END//

DELIMITER ;
/**
 17.2 Probar
**/
/*
CALL p17();
*/


/**
 18.1 Procedimiento p18 que muestra a los numeros de 1 al n-1, siendo n un parametro, con un bucle repeat, en una tabla temporal, para que salgan una sola pesta�a de respuesta.
**/
DELIMITER //

CREATE OR REPLACE 
  PROCEDURE p18(IN n int)
    BEGIN
      DECLARE contador int DEFAULT 0;
      CREATE OR REPLACE TEMPORARY TABLE p18 (
        id int AUTO_INCREMENT,
        num int DEFAULT null,
        PRIMARY KEY (id)
      );
      SET contador = 1;
      REPEAT
        INSERT INTO p18 (num) VALUE (contador);
        SET contador = contador +1;
      UNTIL !(contador < n)
      END REPEAT;
      SELECT num FROM p18;
      DROP TABLE p18;
    END//

DELIMITER ;
/**
 18.2 Probar
**/
CALL p18(16);


/**
 19.1 Procedimiento p18 que muestra a los numeros de 1 al n-1, siendo n un parametro, con un bucle loop, en una tabla temporal, para que salgan una sola pesta�a de respuesta.
**/
DELIMITER //

CREATE OR REPLACE 
  PROCEDURE p19(IN n int)
    BEGIN
      DECLARE contador int DEFAULT 0;
      CREATE OR REPLACE TEMPORARY TABLE p19 (
        id int AUTO_INCREMENT,
        num int DEFAULT null,
        PRIMARY KEY (id)
      );
      SET contador = 1;
      REPEAT
        INSERT INTO p19 (num) VALUE (contador);
        SET contador = contador +1;
      UNTIL !(contador < n)
      END REPEAT;
      SELECT num FROM p19;
      DROP TABLE p19;
    END//

DELIMITER ;
/**
 19.2 Probar
**/
CALL p19(16);


/*

        DECLARE suma int DEFAULT 0;
      DECLARE producto int DEFAULT 0;

      -- Crear la tabla si no existe, si no la deja igual
      CREATE TABLE IF NOT EXISTS sumaproducto (
        id int AUTO_INCREMENT,
        n1 int DEFAULT 0,
        n2 int DEFAULT 0,
        suma int DEFAULT 0,
        producto int DEFAULT 0,
        PRIMARY KEY (id)
      );
/*
CALL p16_while();



  
    18 Pasar un numero guardar enuna tabla los numeros hasta n-1
    19 
  */