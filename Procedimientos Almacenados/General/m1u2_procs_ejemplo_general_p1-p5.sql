﻿-- CREATE DATABASE programacion CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE programacion;




/**
  1.1 Crear un procedimiento almacenado llamado P1 que muestre la fecha de hoy.
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p1()
  COMMENT 'Muestra la fecha de hoy.'
  BEGIN
    SELECT NOW();
  END //

DELIMITER ;


/**
  1.2 Prueba y eliminar el procedimiento almacenado P1.
**/
-- CALL p1();
-- DROP PROCEDURE p1;






/**
  2.1 Crea un procedimiento llamado P2, con una variable de tipo datetime, y almacena
      la fecha de hoy. Después muestra el contenido de dicha variable.
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p2()
  COMMENT 'Muestra la fecha de hoy, tras pasarla a una variable.'
  BEGIN
    -- Declarar
    DECLARE v datetime;
    -- Inicializar
    SET v = NOW();
    -- Mostrar en pantalla
    SELECT v;
  END //

DELIMITER ;

-- 2.2. Llamo el procedimiento
-- CALL p2;





/**
  3.1 Crea el procedimiento P3 que suma 2 numeros que recibe como argumento y me muestra la suma en pantalla
**/

DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p3_sinvars(IN arg1 int, IN arg2 int)
    COMMENT 'Muestra la suma de 2 argumentos enteros.'
    BEGIN
      SELECT arg1 + arg2;
    END //


CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p3_convars(IN arg1 int, IN arg2 int)
    COMMENT 'Muestra la suma de 2 argumentos enteros.'
    BEGIN
      DECLARE suma int DEFAULT 0;
      SET suma = arg1 + arg2;
      SELECT suma;
    END //

DELIMITER ;

/**
  3.2 Prueba las distintas variables de P3.
**/
-- CALL p3_sinvars(5, 4);
-- CALL p3_convars(5, 4);



  
/**
  4.1 Crea in procedimiento P4 que muestre la resta de dos numeros.
**/

DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p4 (IN minuendo int, IN sustraendo int)
    BEGIN
      DECLARE resultado int DEFAULT 0;
      SET resultado = minuendo - sustraendo;
      SELECT resultado;
    END //

DELIMITER ;

/**
  4.2 Prueba de P4
**/
-- CALL p4(3,2);
-- CALL p4(1234567890, 987654321);



/**
  5.1 Crea un procedimiento que almacena los resultados de sumas de 2 numeros en una tabla
**/
DELIMITER //

CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p5_siempre(n1 int, n2 int)
    BEGIN
      -- Declarar variable
      DECLARE resultado int DEFAULT 0;

      -- Crear o borrar tabla
      CREATE TABLE IF NOT EXISTS datos 
      (
        id int AUTO_INCREMENT,
        n1 int DEFAULT 0,
        n2 int DEFAULT 0,
        suma int DEFAULT 0,
        PRIMARY KEY (id)
      );

      -- Declarar salida
      SET resultado = n1 + n2;
      -- 
      INSERT INTO datos (n1, n2, suma) VALUE (n1, n2, resultado);
    END //


CREATE OR REPLACE 
  DEFINER = 'root'@'localhost'
  PROCEDURE p5_opcional(n1 int, n2 int)
    BEGIN
      -- Declarar variable
      DECLARE resultado int DEFAULT NULL;

      -- Crear la tabla si no existe, si no la deja igual
      CREATE TABLE IF NOT EXISTS datos_2 (
        n1 int DEFAULT 0,
        n2 int DEFAULT 0,
        r int DEFAULT 0,
        PRIMARY KEY (n1, n2)
      );

      -- Comprueba si está precalculado
      SELECT d.r INTO resultado FROM datos_2 d WHERE (d.n1 = n1) AND (d.n2 = n2);

      -- Si no lo está, ejecuta e inserta
      IF ISNULL(resultado) THEN BEGIN 
          SET resultado = n1 + n2;
          INSERT INTO datos_2 (n1, n2, r) VALUE (n1, n2, resultado);
        END; 
      END IF;

    END //

DELIMITER ;

/**
  5.2 Prueba los datos
**/
/*
DROP TABLE IF EXISTS datos;
CALL p5_siempre(1,0);
CALL p5_siempre(1,1);
CALL p5_siempre(2,1);
CALL p5_siempre(3,2);
CALL p5_siempre(1,0);
CALL p5_siempre(1,1);
CALL p5_siempre(2,1);
CALL p5_siempre(3,2);
SELECT * FROM datos;

DROP TABLE IF EXISTS datos_2;
CALL p5_opcional(1,0);
CALL p5_opcional(1,1);
CALL p5_opcional(2,1);
CALL p5_opcional(3,2);
CALL p5_opcional(1,0);
CALL p5_opcional(1,1);
CALL p5_opcional(2,1);
CALL p5_opcional(3,2);
SELECT * FROM datos_2;
*/
