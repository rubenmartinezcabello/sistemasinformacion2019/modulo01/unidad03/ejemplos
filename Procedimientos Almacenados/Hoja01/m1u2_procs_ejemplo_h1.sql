/**
 * M�dulo 1 - Bases de datos
 * Unidad 3 - Vistas y programaci�n
 *
 * -- Procedimientos almacenados
 *
 * Ejemplos: Hoja 1, Ejercicio 1
 *
 * Por Rub�n Mart�nez Cabello <runar.orested@gmail.com>
**/



CREATE DATABASE IF NOT EXISTS m1u3_ejemplo1 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;


USE m1u3_ejemplo1;



DELIMITER //

CREATE OR REPLACE PROCEDURE mayorDe2_tablas (m int, n int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE mayorDe2_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO mayorDe2_tablas (num) VALUE (m);
    INSERT IGNORE INTO mayorDe2_tablas (num) VALUE (n);
    SELECT MAX(num) FROM mayorDe2_tablas;
    DROP TABLE mayorDe2_tablas;
  END//

CREATE OR REPLACE PROCEDURE mayorDe2_progr (m int, n int)
  BEGIN
    IF (n > m) 
      THEN SELECT n;
      ELSE SELECT m;
    END IF;
  END//

CREATE OR REPLACE PROCEDURE mayorDe2_funct (m int, n int)
  BEGIN
    SELECT GREATEST(m, n);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE mayorDe3_tablas (m int, n int, o int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE mayorDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO mayorDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MAX(num) FROM mayorDe3_tablas;
    DROP TABLE mayorDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE mayorDe3_progr (m int, n int, o int)
  BEGIN
    IF (n > m) 
      THEN 
        IF (o > n)
          THEN SELECT o;
          ELSE SELECT n;
        END IF;
      ELSE 
        IF (o > m)
          THEN SELECT o;
          ELSE SELECT m;
        END IF;
    END IF;
  END//
CREATE OR REPLACE PROCEDURE mayorDe3_progr (m int, n int, o int)
  BEGIN
    DECLARE mayor int;
    SET mayor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    SELECT mayor;
  END//

CREATE OR REPLACE PROCEDURE mayorDe3_funct (m int, n int, o int)
  BEGIN
    SELECT GREATEST(m, n, o);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE minmaxDe3_tablas (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE minmaxDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO minmaxDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MIN(num) INTO min FROM minmaxDe3_tablas;
    SELECT MAX(num) INTO max FROM minmaxDe3_tablas;
    DROP TABLE minmaxDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE minmaxDe3_progr (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    DECLARE mayor int;
    DECLARE menor int;
    SET mayor = m;
    SET menor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (n<menor) 
      THEN SET menor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    IF (o<menor) 
      THEN SET menor = o;
    END if;
    SET min = menor;
    SET max = mayor;
  END//

CREATE OR REPLACE PROCEDURE minmaxDe3_funct (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    SET min = LEAST(m, n, o);
    SET max = GREATEST(m, n, o);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE daydiff_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT DATEDIFF(future, past);
  END//

CREATE OR REPLACE PROCEDURE daydiff_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT TO_DAYS(future) - TO_DAYS(past);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE monthdiff_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT PERIOD_DIFF( EXTRACT(YEAR_MONTH FROM future), EXTRACT(YEAR_MONTH FROM past) ) ;
  END//

CREATE OR REPLACE PROCEDURE monthdiff_tsdiff (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT TIMESTAMPDIFF(MONTH, past, future);
  END//

CREATE OR REPLACE PROCEDURE monthdiff_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT MONTH(future) - MONTH(past) + 12*(YEAR(future)-YEAR(past));
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE ymddiff_funct(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE y int;
    DECLARE m int;
    DECLARE d int;
    SET y = YEAR(future) - YEAR(past);
    SET m = MONTH(future) - MONTH(past);
    SET d = DAY(future) - DAY(past);
    IF (m<0) 
      THEN BEGIN
          SET m = m+12; 
          SET y = y-1;
        END;
    END IF;
    IF (d<0) 
      THEN BEGIN
          SET d = d + DAY(LAST_DAY(past)); 
          SET m = m-1;
        END;
    END IF;

    SET days = d;
    SET months = m;
    SET years = y;
  END//

CREATE OR REPLACE PROCEDURE ymddiff_tsdiff(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SET years = TIMESTAMPDIFF(YEAR, past, future);
    SET months = TIMESTAMPDIFF(MONTH, past, future);
    SET days = TIMESTAMPDIFF(DAY, past, future);
  END//

DELIMITER ; 
-- --------------------------------------------------------------
DELIMITER //

CREATE OR REPLACE PROCEDURE charcount(IN s varchar(255))
  BEGIN
    SELECT CHAR_LENGTH(s);
  END;

DELIMITER ; 







-- Pruebas de ejercicio 1
/*
CALL mayorDe2_tablas(0, 0);
CALL mayorDe2_progr (0, 0);
CALL mayorDe2_funct (0, 0);

CALL mayorDe2_tablas(3, 2);
CALL mayorDe2_progr (3, 2);
CALL mayorDe2_funct (3, 2);

CALL mayorDe2_tablas(-17, -3);
CALL mayorDe2_progr (-17, -3);
CALL mayorDe2_funct (-17, -3);

CALL mayorDe2_tablas(-4, 2);
CALL mayorDe2_progr (2, -4);
CALL mayorDe2_funct (2, -4);
*/
/*
CALL mayorDe3_tablas(0, 0, 0);
CALL mayorDe3_progr (0, 0, 0);
CALL mayorDe3_funct (0, 0, 0);

CALL mayorDe3_tablas(3, 2, 4);
CALL mayorDe3_progr (3, 2, 4);
CALL mayorDe3_funct (3, 2, 4);

CALL mayorDe3_tablas(-17, -3, 1);
CALL mayorDe3_progr (-17, -3, 1);
CALL mayorDe3_funct (-17, -3, 1);

CALL mayorDe3_tablas(-4, 2, 7);
CALL mayorDe3_progr (7, 2, -4);
CALL mayorDe3_funct (7, -4, 2);
*/

/*
SET @minimo = null;
set @maximo = null;

CALL minmaxDe3_tablas(0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL minmaxDe3_tablas(-4, 2, 7, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_progr (7, 2, -4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL minmaxDe3_funct (7, -4, 2, @minimo, @maximo);
SELECT @minimo, @maximo;

SET @minimo = null;
SET @maximo = null;
*/

/*
CALL daydiff_funct('1978-04-15', '1981-02-23');
CALL daydiff_subst('1978-04-15', '1981-02-23');
*/
/*
CALL monthdiff_funct('1978-04-15', '1981-02-23');
CALL monthdiff_tsdiff('1978-04-15', '1981-02-23');
CALL monthdiff_subst('1978-04-15', '1981-02-23');
*/
/*
SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ymddiff_funct('1978-04-15', '1981-02-23', @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;

SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ymddiff_tsdiff('1978-04-15', '1981-02-23',  @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;
*/
/*
CALL charcount('1234');
CALL charcount('12345678');
CALL charcount('12345678901234567890');
*/
