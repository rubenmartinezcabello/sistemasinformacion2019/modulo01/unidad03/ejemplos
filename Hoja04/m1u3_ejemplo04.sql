/*****************************************************************************
*
*   M�dulo 1 - Unidad 3 - Ejemplo 04
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

CREATE DATABASE IF NOT EXISTS programacion 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;

USE programacion;



DELIMITER //

CREATE OR REPLACE PROCEDURE ej401() -- CONTAINS SQL
  BEGIN
    SELECT "esto es un ejemplo de procedimiento" AS mensaje;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej402() -- CONTAINS SQL
  BEGIN
    DECLARE entero int DEFAULT 1;
    DECLARE cadena varchar(10) DEFAULT NULL;
    DECLARE numero decimal(4,2) DEFAULT 10.48;

    SET entero = entero + 1;
    SET numero = numero * 0.66;
    SET cadena = "precio";
    SELECT CONCAT(UPPER(LEFT(cadena,1)), SUBSTR(cadena, 2), "=", (entero*numero)) AS mensaje;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej403() -- CONTAINS SQL
  BEGIN
    DECLARE v_caracter1 char(1);
    DECLARE forma_pago enum ('metalico', 'tarjeta', 'transferencia');
    SET v_caracter1 = 'hola';
    SET forma_pago = 'TARJETA';
  SELECT v_caracter1, forma_pago;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej404(IN valor float) -- CONTAINS SQL
  BEGIN
    SELECT valor;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej405(IN valor float) -- CONTAINS SQL
  BEGIN
    DECLARE variable float DEFAULT NULL;
    SET variable = valor;
    SELECT variable;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej406(IN valor float) -- CONTAINS SQL
  BEGIN
    DECLARE signo varchar(8) DEFAULT null;
  
  IF valor <0 
      THEN SET signo = 'negativo';
    ELSEIF valor >0
      THEN SET signo= 'negativo';
    ELSE
      SET signo = 'cero';
    END if;

    SELECT signo;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION ej407(valor int) RETURNS BOOLEAN
  BEGIN
    RETURN IF(valor MOD 2, false, true);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION ej408(cateto1 float, cateto2 float) RETURNS float
  BEGIN
    RETURN SQRT( POWER(cateto1, 2) * POWER(cateto2, 2) );
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej409(IN valor float, OUT signo varchar(8)) -- CONTAINS SQL
  BEGIN
  IF valor <0 
      THEN SET signo = 'negativo';
    ELSEIF valor >0
      THEN SET signo= 'negativo';
    ELSE
      SET signo = 'cero';
    END if;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej410_if(IN nota float) -- CONTAINS SQL
  BEGIN
    IF (nota<0) THEN SELECT "nota no valida" AS nota;
    ELSEIF (nota)<5 THEN SELECT "INSUFICIENTE" AS nota;
    ELSEIF (nota)<6 then SELECT "SUFICIENTE" AS nota;
    ELSEIF (nota)<7 then SELECT "BIEN" AS nota;
    ELSEIF (nota)<9 then SELECT "NOTABLE" AS nota;
    ELSEIF (nota)<=10 then SELECT "SOBRESALIENTE" AS nota;
    ELSE SELECT "nota no valida" AS nota;
    END IF;
  END//


CREATE OR REPLACE PROCEDURE ej410_case(IN varnota float) -- CONTAINS SQL
  BEGIN
    CASE  
      WHEN varnota<0 THEN SELECT "nota no valida" AS nota;
      WHEN varnota<=5 THEN SELECT "INSUFICIENTE" AS nota;
      WHEN varnota<=6 then SELECT "SUFICIENTE" AS nota;
      WHEN varnota<=7 then SELECT "BIEN" AS nota;
      WHEN varnota<=9 then SELECT "NOTABLE" AS nota;
      WHEN varnota<=10 then SELECT "SOBRESALIENTE" AS nota;
      ELSE SELECT "nota no valida" AS nota;
    END CASE;
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej411(IN fullname varchar(50), IN grade float) -- CONTAINS SQL
  BEGIN
    CREATE TABLE IF NOT EXISTS notas(
        id int AUTO_INCREMENT,
        nombre varchar(50),
        nota float,
        PRIMARY KEY(id)
      ) ENGINE=INNODB;

    INSERT INTO notas (nombre, nota) VALUE (fullname, grade); 
    --  ON DUPLICATE KEY UPDATE set nota=grade; si fullname fuese la primary key
  END//


CREATE OR REPLACE FUNCTION ej412(weekdayNum int) RETURNS varchar(15)
  BEGIN
    CASE ((weekdayNum MOD 7) +7) MOD 7
      WHEN 0 THEN RETURN "Domingo";
      WHEN 1 THEN RETURN "Lunes";
      WHEN 2 THEN RETURN "Martes";
      WHEN 3 THEN RETURN "Mi�rcoles";
      WHEN 4 THEN RETURN "Jueves";
      WHEN 5 THEN RETURN "Viernes";
      WHEN 6 THEN RETURN "S�bado";
      ELSE RETURN "NO VALIDO";
    END CASE;
  END//


-- Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo sin utilizar cursores. 
SELECT * FROM alumnos a;
  -- NO HAY NOMBRES EN LA TABLA DE ALUMNOS
/*
  
12.-  Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo sin utilizar cursores. 
14.-  Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo utilizando cursores (sin manejar excepciones). 
15.-  Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo utilizando cursores (manejando excepciones). 
16.-  Crear un procedimiento almacenado que me cree una tabla denominada usuarios solamente si no existe. 
La tabla debe ser: 
- Id: num�rico, autoincremental, clave principal - Nombre: texto de 20 caracteres - Contrase�a: texto de 20 caracteres - nombreUsuario: texto de 20 caracteres (indexado sin duplicado) 
17.-  Crear un procedimiento almacenado que me inserte datos en la tabla usuarios. 
18.-  Crear un procedimiento almacenado que llame a los procedimientos almacenados de los dos ejercicios anteriores.  
19.-  Crear una funci�n que le pasemos un argumento de tipo texto (ser� el nombre de usuario) y me devuelva verdadero en caso de que el usuario exista en la tabla usuarios y FALSO en caso de que no. 
 
 


/*
CREATE OR REPLACE PROCEDURE ej101_if(IN n1 int, IN n2 int)
  -- CONTAINS SQL
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa instrucciones de control de flujo IF...THEN)'
  BEGIN
    DECLARE s int DEFAULT 0; 
    SET s = n1;
    IF (n2>s) 
      THEN 
        SET s = n2;
    END IF;
    SELECT s;
  END//

CREATE OR REPLACE PROCEDURE ej101_totales (IN n1 int, IN n2 int)
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa funciones de totales de tabla)'
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE t_ej101_totales (n int);
    INSERT IGNORE INTO t_ej101_totales (n) VALUE (n1);
    INSERT IGNORE INTO t_ej101_totales (n) VALUE (n2);
    SELECT MAX(n) FROM t_ej101_totales;
    DROP TABLE t_ej101_totales;
  END//

CREATE OR REPLACE PROCEDURE ej101_func(IN n1 int, IN n2 int)
  COMMENT 'Procedimiento almacenado que imprime el mayor de los dos enteros que les pases. (Usa funcion GREATEST())'
  BEGIN
    SELECT GREATEST(n1, n2);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej102_tablas (m int, n int, o int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE mayorDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO mayorDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MAX(num) FROM mayorDe3_tablas;
    DROP TABLE mayorDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE ej102_progr (m int, n int, o int)
  BEGIN
    IF (n > m) 
      THEN 
        IF (o > n)
          THEN SELECT o;
          ELSE SELECT n;
        END IF;
      ELSE 
        IF (o > m)
          THEN SELECT o;
          ELSE SELECT m;
        END IF;
    END IF;
  END//
CREATE OR REPLACE PROCEDURE ej102_progr (m int, n int, o int)
  BEGIN
    DECLARE mayor int;
    SET mayor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    SELECT mayor;
  END//

CREATE OR REPLACE PROCEDURE ej102_funct (m int, n int, o int)
  BEGIN
    SELECT GREATEST(m, n, o);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej103_tablas (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE minmaxDe3_tablas (
      num int,
      PRIMARY KEY (num)
    );
    INSERT IGNORE INTO minmaxDe3_tablas (num) VALUES (m), (n), (o);
    SELECT MIN(num) INTO min FROM minmaxDe3_tablas;
    SELECT MAX(num) INTO max FROM minmaxDe3_tablas;
    DROP TABLE minmaxDe3_tablas;
  END//

CREATE OR REPLACE PROCEDURE ej103_progr (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    DECLARE mayor int;
    DECLARE menor int;
    SET mayor = m;
    SET menor = m;
    IF (n>mayor) 
      THEN SET mayor = n;
    END if;
    IF (n<menor) 
      THEN SET menor = n;
    END if;
    IF (o>mayor) 
      THEN SET mayor = o;
    END if;
    IF (o<menor) 
      THEN SET menor = o;
    END if;
    SET min = menor;
    SET max = mayor;
  END//

CREATE OR REPLACE PROCEDURE ej103_funct (IN m int, IN n int, IN o int, OUT min int, OUT max int)
  BEGIN
    SET min = LEAST(m, n, o);
    SET max = GREATEST(m, n, o);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej104_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT DATEDIFF(future, past);
  END//

CREATE OR REPLACE PROCEDURE ej104_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT TO_DAYS(future) - TO_DAYS(past);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej105_funct (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT PERIOD_DIFF( EXTRACT(YEAR_MONTH FROM future), EXTRACT(YEAR_MONTH FROM past) ) ;
  END//

CREATE OR REPLACE PROCEDURE ej105_tsdiff (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    
    SELECT TIMESTAMPDIFF(MONTH, past, future);
  END//

CREATE OR REPLACE PROCEDURE ej105_subst (IN past date, IN future date)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SELECT MONTH(future) - MONTH(past) + 12*(YEAR(future)-YEAR(past));
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej106_funct(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE y int;
    DECLARE m int;
    DECLARE d int;
    SET y = YEAR(future) - YEAR(past);
    SET m = MONTH(future) - MONTH(past);
    SET d = DAY(future) - DAY(past);
    IF (m<0) 
      THEN BEGIN
          SET m = m+12; 
          SET y = y-1;
        END;
    END IF;
    IF (d<0) 
      THEN BEGIN
          SET d = d + DAY(LAST_DAY(past)); 
          SET m = m-1;
        END;
    END IF;

    SET days = d;
    SET months = m;
    SET years = y;
  END//

CREATE OR REPLACE PROCEDURE ej106_tsdiff(IN past date, IN future date, OUT years int , OUT months int , OUT days int)
  BEGIN
    DECLARE aux date;
    IF (past>future)
      THEN BEGIN
        SET aux = future;
        SET future = past;
        SET past = aux;
      END;
    END IF;
    SET years = TIMESTAMPDIFF(YEAR, past, future);
    SET months = TIMESTAMPDIFF(MONTH, past, future);
    SET days = TIMESTAMPDIFF(DAY, past, future);
  END//

-- -----------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ej107(IN s varchar(255))
  BEGIN
    SELECT CHAR_LENGTH(s);
  END//
*/

DELIMITER ; 



-- Pruebas de ejercicio 4
/*

CALL ej401();

CALL ej402();

CALL ej403();

CALL ej404(1.0);

CALL ej405(2.0);

CALL ej406(-2.0);

SELECT IF(ej407(4),"PAR","IMPAR");

SELECT ej408(3,4);

CALL ej409(-3, @lastresult);
SELECT @lastresult;

CALL ej410_if(-2);
CALL ej410_if(0);
CALL ej410_if(3.25);
CALL ej410_if(5);
CALL ej410_if(6.3);
CALL ej410_if(7.8);
CALL ej410_if(9.15);
CALL ej410_if(10);
CALL ej410_if(11);

CALL ej410_case(-2);
CALL ej410_case(0);
CALL ej410_case(3.25);
CALL ej410_case(5);
CALL ej410_case(6.3);
CALL ej410_case(7.8);
CALL ej410_case(9.15);
CALL ej410_case(10);
CALL ej410_case(11);


SELECT ej411("Paco Pil", 3.5);

SELECT ej412(-9);
SELECT ej412(-8);
SELECT ej412(-7);
SELECT ej412(-6);
SELECT ej412(-5);
SELECT ej412(-3);
SELECT ej412(-1);
SELECT ej412(0);
SELECT ej412(1);
SELECT ej412(2);
SELECT ej412(3);
SELECT ej412(4);
SELECT ej412(5);
SELECT ej412(6);
SELECT ej412(7);*/

SELECT now();
SELECT WEEKDAY(now());
SELECT ej412( (WEEKDAY(now())+1) MOD 7 );

/*



CALL ej101_totales(0, 0);
CALL ej101_if (0, 0);
CALL ej101_func (0, 0);

CALL ej101_totales(-17, -3);
CALL ej101_if (-17, -3);
CALL ej101_func (-17, -3);

CALL ej102_tablas(0, 0, 0);
CALL ej102_progr (0, 0, 0);
CALL ej102_funct (0, 0, 0);

CALL ej102_tablas(3, 2, 4);
CALL ej102_progr (3, 2, 4);
CALL ej102_funct (3, 2, 4);



SET @minimo = null;
set @maximo = null;

CALL ej103_tablas(0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (0, 0, 0, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (3, 2, 4, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (-17, -3, 1, @minimo, @maximo);
SELECT @minimo, @maximo;

CALL ej103_tablas(-4, 2, 7, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_progr (7, 2, -4, @minimo, @maximo);
SELECT @minimo, @maximo;
CALL ej103_funct (7, -4, 2, @minimo, @maximo);
SELECT @minimo, @maximo;

SET @minimo = null;
SET @maximo = null;



CALL ej104_funct('1978-04-15', '1981-02-23');
CALL ej104_subst('1978-04-15', '1981-02-23');

CALL ej105_funct('1978-04-15', '1981-02-23');
CALL ej105_tsdiff('1978-04-15', '1981-02-23');
CALL ej105_subst('1978-04-15', '1981-02-23');



SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ej106_funct('1978-04-15', '1981-02-23', @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;

SET @dia = null;
SET @mes = null;
SET @anno = null;
CALL ej106_tsdiff('1978-04-15', '1981-02-23',  @anno, @mes, @dia);
SELECT @dia, @mes,  @anno;


CALL ej107('1234');
CALL ej107('12345678');
CALL ej107('12345678901234567890');
*/