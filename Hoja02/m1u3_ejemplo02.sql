/*****************************************************************************
*
*   M�dulo 1 - Unidad 3 - Ejemplo 02
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

CREATE DATABASE IF NOT EXISTS programacion 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;

USE programacion;


-- A�adir datos
CREATE OR REPLACE TABLE datos 
(  
  datos_id INT(11) NOT NULL AUTO_INCREMENT,
  numero1 INT(11) NOT NULL,
  numero2 INT(11) NOT NULL,
  suma VARCHAR(255) DEFAULT NULL,
  resta VARCHAR(255) DEFAULT NULL,
  rango VARCHAR(5) DEFAULT NULL,
  texto1 VARCHAR(25) DEFAULT NULL,
  texto2 VARCHAR(25) DEFAULT NULL,
  junto VARCHAR(255) DEFAULT NULL,
  longitud VARCHAR(255) DEFAULT NULL,
  tipo INT(11) NOT NULL,
  numero INT(11) NOT NULL,
  PRIMARY KEY (datos_id)
) ENGINE = INNODB, COMMENT = 'Esta tabla esta para poder actualizarla con los procedimientos', ROW_FORMAT = Compact, TRANSACTIONAL = 0; 

ALTER TABLE datos 
  ADD INDEX numero2_index(numero2); 
ALTER TABLE datos
  ADD INDEX numero_index(numero); 
ALTER TABLE datos 
  ADD INDEX tipo_index(tipo);












DELIMITER //

-- 1.- Realizar un procedimiento almacenado que reciba un texto y un car�cter. Debe indicarte si ese car�cter est� en el texto. Deb�is realizarlo con: Locate, Position. 

CREATE OR REPLACE PROCEDURE ej201_locate(IN texto varchar(255), IN caracter char(1))
  -- CONTAINS SQL
  BEGIN
    DECLARE r varchar(5) DEFAULT 'FALSE'; 
    IF LOCATE(caracter, texto)<>0
      THEN 
        SET r = 'TRUE';
    END IF;
    SELECT r;
  END//

CREATE OR REPLACE PROCEDURE ej201_position(IN texto varchar(255), IN caracter char(1))
  -- CONTAINS SQL
  BEGIN
    DECLARE r varchar(5) DEFAULT 'FALSE'; 
    IF POSITION(caracter IN texto) <> 0
      THEN 
        SET r = 'TRUE';
    END IF;
    SELECT r;
  END//


-- 2.-  Realizar un procedimiento almacenado que reciba un texto y un car�cter. Te debe indicar todo el texto que haya antes de la primera vez que aparece ese car�cter.  
CREATE OR REPLACE PROCEDURE ej202_endchar(IN texto varchar(255), IN caracter char(1))
  -- CONTAINS SQL
  BEGIN
    DECLARE pos int DEFAULT 0; 
    SET pos = LOCATE(caracter, texto);
    IF pos = 0
      THEN 
        SELECT texto;
      ELSE 
         SELECT SUBSTRING(texto, 1, pos);
    END IF;
  END//


-- 3.-  Realizar un procedimiento almacenado que reciba tres n�meros y dos argumentos de tipo salida donde devuelva el n�mero m�s grande y el n�mero m�s peque�o de los tres n�meros pasados 
CREATE OR REPLACE PROCEDURE ej203_maxmin(IN n1 int, IN n2 int, IN n3 int, OUT max int, OUT min int)
  -- CONTAINS SQL
  BEGIN
    SET max = GREATEST(n1, n2, n3);
    SET min = LEAST(n1,n2,n3);
  END//

-- 4.-  Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 son mayores que 50  
CREATE OR REPLACE PROCEDURE ej204()
  BEGIN
    DECLARE numeros1 int DEFAULT 0;
    DECLARE numeros2 int DEFAULT 0;
    SET numeros1 = (SELECT COUNT(*) FROM datos d WHERE d.numero1 > 50);
    SET numeros2 = (SELECT COUNT(*) FROM datos d WHERE d.numero2 > 50);
    SELECT numeros1+numeros2;
  END//

-- 5.-  Realizar un procedimiento almacenado que calcule la suma y la resta de numero1 y numero2 y actualice la tabla
CREATE OR REPLACE PROCEDURE ej205()
  BEGIN
    UPDATE datos SET suma = numero1+numero2, resta = numero2-numero1 WHERE TRUE;
  END//


-- 6.-  Realizar un procedimiento almacenado que primero ponga todos los valores de suma y resta a NULL y depues calcule la suma solamente si el numero1 es mayor que el numero2 y calcule la resta de numero2-numero1 si el numero2 es mayor o numero1numero2 si es mayor el numero1  
CREATE OR REPLACE PROCEDURE ej206_3pasadas()
  BEGIN
    UPDATE datos SET suma = NULL, resta = NULL WHERE TRUE;
    UPDATE datos SET suma = numero1+numero2 WHERE numero1>numero2;
    UPDATE datos SET resta = numero2-numero1 WHERE numero2>numero1;
  END//

CREATE OR REPLACE PROCEDURE ej206_1pasadas()
  BEGIN
    UPDATE datos SET suma = IF(numero1>numero2, numero1+numero2, NULL), resta = IF(numero2>numero1, numero2-numero1, NULL) WHERE TRUE;
  END//

-- 7.-  Realizar un procedimiento almacenado que coloque en el campo junto el texto1, texto2 
CREATE OR REPLACE PROCEDURE ej207()
  BEGIN
    UPDATE datos SET junto = CONCAT(texto1, " ", texto2) WHERE TRUE;
  END//

-- 8.-  Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL. Despu�s debe colocar en el campo junto el texto1-texto2 si el rango es A y si es rango B debe colocar texto1+texto. Si el rango es distinto debe colocar texto1 nada m�s. 
CREATE OR REPLACE PROCEDURE ej208_4pasadas()
  BEGIN
    UPDATE datos SET junto = NULL WHERE TRUE;
    UPDATE datos SET junto=texto1 WHERE TRUE;
    UPDATE datos SET junto=CONCAT(texto1,'-',texto2) WHERE rango='a';
    UPDATE datos SET junto=CONCAT(texto1,'+',texto2) WHERE rango='b';
  END//

CREATE OR REPLACE PROCEDURE ej208_2pasadas()
  BEGIN
    UPDATE datos SET junto = NULL WHERE TRUE;
    UPDATE datos SET junto=texto1 WHERE TRUE;
    UPDATE datos SET junto=IF(rango='a', CONCAT(texto1,'-',texto2), IF( rango='b', CONCAT(texto1,'+',texto2),texto1)) WHERE TRUE;
  END//


DELIMITER ;


-- Pruebas de ejercicio 2

INSERT INTO datos(numero1, numero2, rango, texto1, texto2) VALUES 
(  35,  23,'A', "copenhage", "femur" ), 
(  12,  34,'B', "pecador", "estafilococo" ), 
( 857,  63,'C', "fistro", "leucocito" ),
( -56, -88,'A', "neumococo", "epi" ), 
( 342, -45,'B', "blas", "espinete" ), 
( -58,+443,'C', "pin", "chapas" );

CALL ej201_locate('Los d�gitos son 0,1,2,3,4,5,6,7,8 y 9', '7');
CALL ej201_locate('Los d�gitos son 0,1,2,3,4,5,6,7,8 y 9', 'K');

CALL ej202_endchar('Los d�gitos son 0,1,2,3,4,5,6,7,8 y 9', '5');

SET @n1 = 2341; 
SET @n2 = -132;
SET @n3 = 90814;
SET @max = NULL;
SET @min = NULL;
CALL ej203_maxmin(@n1,@n2,@n3,@max,@min);
SELECT @min, @max;

CALL ej204();

CALL ej205();
SELECT * FROM datos;

CALL ej206_3pasadas();
SELECT * FROM datos;

CALL ej206_1pasadas();
SELECT * FROM datos;

CALL ej207();
SELECT * FROM datos;

CALL ej208_4pasadas();
SELECT * FROM datos;
CALL ej208_2pasadas();
SELECT * FROM datos;