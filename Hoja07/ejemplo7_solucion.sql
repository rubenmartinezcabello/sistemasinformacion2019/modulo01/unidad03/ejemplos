﻿/**
  Ejemplo 7
  Disparadores y errores
**/

/** 
  Modificamos las tablas clientes y provincias
**/

  ALTER TABLE clientes ADD COLUMN edad int DEFAULT 0;
  ALTER TABLE provincias ADD COLUMN cantidad int DEFAULT 0;
  ALTER TABLE provincias ADD COLUMN iniciales char(1);

/** 
  Ejercicio 1
  Crear un disparador para que cuando inserte un registro en clientes 
  me compruebe si la edad esta entre 18 y 70. 
  En caso de que no esté produzca una excepción con el mensaje
  “La edad no es válida”.
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBI
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
  /** 
    Ejercicio 1
    Crear un disparador para que cuando inserte un registro en clientes 
    me compruebe si la edad esta entre 18 y 70. 
    En caso de que no esté produzca una excepción con el mensaje
    “La edad no es válida”.
  **/
    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La edad no es valida";
   END IF;
  END //
  
  DELIMITER ;

-- probando el disparador
  INSERT INTO clientes (CodCli, NombCli, edad)
    VALUES (979,'nuevo1', 65);

  SELECT * FROM clientes;

-- modificando un poco (RESIGNAL)
  DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBI
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000' 
      BEGIN
          RESIGNAL SET MESSAGE_TEXT="La edad no es valida";
      END;
    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      SIGNAL SQLSTATE '45000';
   END IF;
  END //
  
  DELIMITER ;


/**
  Ejercicio 2
  Además, cada vez que un usuario introduzca un registro 
  con una edad no valida debe grabar ese registro en una 
  tabla denominada errores.
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBI
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
  /**
    Ejercicio 2
    Además, cada vez que un usuario introduzca un registro 
    con una edad no valida debe grabar ese registro en una 
    tabla denominada errores.
  **/
    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      INSERT INTO errores (mensaje, fecha, hora)
        VALUES ('la edad no es valida', CURDATE(), CURTIME());
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT ="La edad no es valida";
      
   END IF;
  END //
DELIMITER ;

-- probando el disparador
  INSERT INTO clientes (CodCli, NombCli, edad)
    VALUES (980,'nuevo2', 85);

  SELECT * FROM clientes;
  SELECT * FROM errores;


/** 
  Ejercicio 3
  Crear un disparador para que cuando modifique un registro de la tabla 
  clientes me compruebe si la edad esta entre 18 y 70. 
  En caso de que no esté produzca una excepción con el mensaje 
  “La edad no es válida”.
**/  

DELIMITER //
CREATE OR REPLACE TRIGGER clientesBU
BEFORE UPDATE
  ON clientes
  FOR EACH ROW
BEGIN
  IF(new.edad NOT BETWEEN 18 AND 70) THEN
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La edad no es valida";
   END IF;
END //

DELIMITER ;


/**
  Ejercicio 4
  Crear disparadores en la tabla localidades que comprueben que 
  cuando introduzcas o modifiques una localidad el código de la 
  provincia exista en la tabla provincias. En caso de que no exista 
  que cree la provincia. El resto de campos de la tabla provincias se
  colocarán a valor por defecto. 
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER localidadesBI
  BEFORE INSERT
    ON localidades
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    -- primera opcion
    
    SELECT COUNT(*) INTO numero FROM provincias p 
      WHERE p.CodPro=new.CodPro;
    
    IF(numero=0) THEN
      INSERT INTO provincias (CodPro)
        VALUES (new.CodPro);
    END IF;

    -- segundo opcion

    /*IF(new.codPro NOT IN 
        (SELECT p.CodPro FROM provincias p)
        ) THEN
      INSERT INTO provincias (CodProd)
        VALUES (new.CodPro);
    END IF;*/
    
  END //
  
  DELIMITER ;

INSERT INTO localidades (CodLoc, NombLoc, CodPro)
  VALUES ('10002', 'potes', '54');

SELECT * FROM provincias p;



DELIMITER //
  CREATE OR REPLACE TRIGGER localidadesBI
  BEFORE INSERT
    ON localidades
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
      BEGIN
        INSERT INTO provincias (CodProd)
        VALUES (new.CodPro);

      END;
    
    SELECT COUNT(*) INTO numero FROM provincias p 
      WHERE p.CodPro=new.CodPro;
    
    IF(numero=0) THEN
      SIGNAL SQLSTATE '45000';
    END IF;
  END //
  
  DELIMITER ;

-- para actualizar

  DELIMITER //
  CREATE OR REPLACE TRIGGER localidadesBU
  BEFORE UPDATE
    ON localidades
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
           
      SELECT COUNT(*) INTO numero FROM provincias p 
        WHERE p.CodPro=new.CodPro;
      
      IF(numero=0) THEN
        INSERT INTO provincias (CodProd)
          VALUES (new.CodPro);
      END IF;

   
  END //
  
  DELIMITER ;

/**
  Ejercicio 5
  Modificar los disparadores de la tabla clientes para que 
  cuando introduzcas o modifiques un cliente compruebe 
  si la localidad del cliente exista. 
  En caso que no exista mostrar el mensaje de error 
  “La localidad no existe”. 
  Debe insertar ese registro de error en la tabla errores.
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBI
  BEFORE INSERT
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;

    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      INSERT INTO errores (mensaje, fecha, hora)
        VALUES ('la edad no es valida', CURDATE(), CURTIME());
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La edad no es valida";
    END IF;

    -- comprobar si la localidad existe
    SELECT COUNT(*) INTO numero FROM localidades l 
      WHERE l.CodLoc=new.CodLoc;

    IF (numero=0) THEN
      INSERT INTO errores (mensaje, fecha, hora)
        VALUES ('la localidad no existe', CURDATE(), CURTIME());
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La localidad no existe";
    END IF;

  END //
  
  DELIMITER ;

INSERT INTO clientes (CodCli, CodLoc, edad)
  VALUES (897,'SSS', 90);


 DELIMITER //
  CREATE OR REPLACE TRIGGER clientesBU
  BEFORE UPDATE
    ON clientes
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;

    IF(new.edad NOT BETWEEN 18 AND 70) THEN
      INSERT INTO errores (mensaje, fecha, hora)
        VALUES ('la edad no es valida', CURDATE(), CURTIME());
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La edad no es valida";
    END IF;

    -- comprobar si la localidad existe
    SELECT COUNT(*) INTO numero FROM localidades l 
      WHERE l.CodLoc=new.CodLoc;

    IF (numero=0) THEN
      INSERT INTO errores (mensaje, fecha, hora)
        VALUES ('la localidad no existe', CURDATE(), CURTIME());
      SIGNAL SQLSTATE '45000'  SET MESSAGE_TEXT ="La localidad no existe";
    END IF;

  END //
  
  DELIMITER ;

/**
  Ejercicio 6
  Crear unos disparadores para la tabla provincias que cuando 
  modifiques o insertes un registro te realice algunas operaciones.
  
  Las operaciones son las siguientes:
    - Te calcule las iniciales de la provincia introducida
    - Te compruebe si la provincia por error se haya utilizado 
      ya en localidades. En caso de que si, debe calcular el número
      de localidades que tiene la provincia.

**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER provinciasBI
  BEFORE INSERT
    ON provincias
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    -- obtener las iniciales
    SET new.iniciales=LEFT(new.NombPro,1);
    
    -- calculases la cantidad de localidades
    -- de esa provincia
    SELECT COUNT(*) INTO numero FROM localidades l 
      WHERE l.CodPro=new.CodPro;
    
    SET new.cantidad=numero;

  END //
  
  DELIMITER ;

-- para las actualizaciones

  DELIMITER //
  CREATE OR REPLACE TRIGGER provinciasBU
  BEFORE UPDATE
    ON provincias
    FOR EACH ROW
  BEGIN
    DECLARE numero int DEFAULT 0;
    -- obtener las iniciales
    SET new.iniciales=LEFT(new.NombPro,1);
    
    -- calculases la cantidad de localidades
    -- de esa provincia
    SELECT COUNT(*) INTO numero FROM localidades l 
      WHERE l.CodPro=new.CodPro;
    
    SET new.cantidad=numero;

  END //
  
  DELIMITER ;

/**
  Ejercicio 7
  Modificar los disparadores para la tabla localidades 
  para que cuando insertes o modifiques un registro te 
  recalcule el campo cantidad de la tabla provincias.
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER localidadesAI
  AFTER INSERT
    ON localidades
    FOR EACH ROW
  BEGIN
    DECLARE numero int;
    
    -- numero de localidades de la provincia
    -- a la que pertenece la localidad nueva
    SELECT COUNT(*) INTO numero FROM localidades 
            WHERE CodPro=new.CodPro;

    -- almaceno el numero en la tabla provincias
    UPDATE provincias 
      SET cantidad=numero
      WHERE CodPro=new.CodPro;

  END //
  
  DELIMITER ;

DELIMITER //
  CREATE OR REPLACE TRIGGER localidadesAU
  AFTER UPDATE
    ON localidades
    FOR EACH ROW
  BEGIN
    DECLARE numero int;
    
    -- numero de localidades de la provincia
    -- a la que pertenece la localidad nueva
    SELECT COUNT(*) INTO numero FROM localidades 
            WHERE CodPro=new.CodPro;

    -- almaceno el numero en la tabla provincias
    UPDATE provincias 
      SET cantidad=numero
      WHERE CodPro=new.CodPro;


    SELECT COUNT(*) INTO numero FROM localidades 
            WHERE CodPro=old.CodPro;

    UPDATE provincias 
      SET cantidad=numero
      WHERE CodPro=old.CodPro;

  END //
  
  DELIMITER ;