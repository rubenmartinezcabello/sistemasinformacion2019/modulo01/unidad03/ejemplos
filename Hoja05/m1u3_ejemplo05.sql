/*****************************************************************************
*
*   M�dulo 1 - Unidad 3 - Ejemplo 05
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

USE jardineria;


DELIMITER //

-- 01 - Escribe un procedimiento que reciba el nombre de un pa�s como par�metro de entrada y realice una consulta sobre la tabla cliente para obtener todos los clientes que existen en la tabla de ese pa�s.
CREATE OR REPLACE PROCEDURE ej501( IN pais varchar(50) )
  BEGIN
    SELECT * FROM cliente cli WHERE cli.pais = pais;
  END//


-- 02 - Realizar una funci�n que me devuelva el n�mero de registros de la tabla clientes que pertenezcan a un pa�s que le pase por teclado. 
CREATE OR REPLACE FUNCTION ej502( teclado varchar(50) ) RETURNS int
  BEGIN
    DECLARE numcli int DEFAULT 0;
    SELECT COUNT(*) INTO numcli FROM cliente cli WHERE cli.pais = teclado;
    RETURN numcli;
  END//

-- 03 - Realizar el mismo ejercicio anterior, pero para desplazarme por la tabla utilizar cursores con excepciones. 
-- NO SE HAN EXPLICADO CURSORES Y EXCEPCIONES

-- 04 - Escribe una funci�n que le pasas una cantidad y que te devuelva el n�mero total de productos que hay en la tabla productos cuya cantidad en stock es superior a la pasada. 
CREATE OR REPLACE FUNCTION ej504( cantidad int ) RETURNS int
  BEGIN
    DECLARE numprods int DEFAULT 0;
    SELECT COUNT(*) INTO numprods FROM producto p WHERE p.cantidad_en_stock > cantidad;
    RETURN numprods;
  END//

-- 05 - Realizar el mismo ejercicio anterior pero con cursores 
-- NO SE HAN EXPLICADO CURSORES Y EXCEPCIONES

-- 06 - Escribe un procedimiento que reciba como par�metro de entrada una forma de pago, que ser� 
-- una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva como salida el pago 
-- de m�ximo valor realizado para esa forma de pago. Deber� hacer uso de la tabla pago de la base 
-- de datos jardiner�a. 
CREATE OR REPLACE PROCEDURE ej506( IN paymentForm varchar(40), OUT maxPayment decimal(15,2))
  BEGIN
    SET maxPayment = 0;
    SELECT MAX(total) INTO maxPayment
      FROM pago p 
      WHERE forma_pago = paymentForm;
  END//


-- 07 - Escribe un procedimiento que reciba como par�metro de entrada una forma de pago, que ser� 
-- una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva como salida los 
-- siguientes valores teniendo en cuenta la forma de pago seleccionada como par�metro de entrada: 
-- - el pago de m�ximo valor,
-- - el pago de m�nimo valor,
-- - el valor medio de los pagos realizados,
-- - la suma de todos los pagos, 
-- - el n�mero de pagos realizados para esa forma de pago. 

CREATE OR REPLACE PROCEDURE ej507( IN paymentForm varchar(40), OUT nmin decimal(15,2), OUT nmax decimal(15,2), OUT nmed decimal(15,2), OUT nsum decimal(15,2), OUT ncou int )
  BEGIN
    SELECT MIN(total), MAX(total), AVG(total), SUM(total), COUNT(total) INTO nmin, nmax, nmed, nsum, ncou
      FROM pago p 
      WHERE forma_pago = paymentForm;
  END//


-- 08 - Escribe una funci�n para la tabla productos que devuelva el valor medio del precio de venta de los productos de un determinado proveedor que se recibir� como par�metro de entrada. El par�metro de entrada ser� el nombre del proveedor. 
CREATE OR REPLACE FUNCTION ej508 (pr varchar(50)) RETURNS decimal(15,2)
  BEGIN
    DECLARE val float DEFAULT 0;
    SELECT AVG(p.precio_venta) INTO val FROM producto p GROUP BY p.proveedor HAVING p.proveedor = pr;
    RETURN val;
  END//

-- 09 - Realizar el mismo ejercicio anterior, pero haciendo que en caso de que el fabricante pasado no tenga productos produzca una excepci�n personalizada que muestre el error "fabricante X no existe".
-- NO SE HAN EXPLICADO CURSORES Y EXCEPCIONES


DELIMITER ;



CALL ej501('Spain');
SELECT ej502('Spain');

CALL ej504(120);

SELECT * FROM pago p;
CALL ej506('PayPal', @maximo);
SELECT 'PayPal', @maximo;

CALL ej507('PayPal', @mi, @ma, @me, @to, @co);
SELECT 'PayPal', @mi, @ma, @me, @to, @co;
*/
SELECT * FROM producto p HAVING p.proveedor = "Murcia Seasons";
SELECT ej508("Murcia Seasons");



