USE ciclistas;


/* Query con subqueries complejas. */
SELECT c.nombre, c.edad
  FROM (
    SELECT DISTINCT e.dorsal
      FROM etapa e
    ) ganadores
    JOIN 
    ciclista c 
    USING (dorsal);


/* Crear la subquery dorsales de ganadores de etapa */
CREATE OR REPLACE VIEW ganadores AS 
  SELECT DISTINCT e.dorsal
    FROM etapa e
;

/* Probando dorsales de ganadores de etapa */
SELECT * FROM ganadores;


/* Reformar la query compleja con vistas */
SELECT c.nombre, c.edad
  FROM 
    ganadores g
    JOIN 
    ciclista c 
    USING (dorsal);
