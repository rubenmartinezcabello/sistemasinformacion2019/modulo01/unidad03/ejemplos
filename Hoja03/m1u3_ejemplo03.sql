/*****************************************************************************
*
*   M�dulo 1 - Unidad 3 - Ejemplo 03
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

CREATE DATABASE IF NOT EXISTS programacion 
  CHARACTER SET utf8mb4 
  COLLATE utf8mb4_spanish_ci;

USE programacion;

DELIMITER //

-- 01 - Importar datos de excel



-- 02 - Funci�n �rea triangulo 
CREATE OR REPLACE FUNCTION areaTriangulo ( base float, altura float ) RETURNS float
  BEGIN
    RETURN (base * altura / 2);
  END//
-- 03 - Funci�n perimetro triangulo 
CREATE OR REPLACE FUNCTION perimetroTriangulo ( lado1 float, lado2 float, lado3 float ) RETURNS float
  BEGIN
    RETURN (lado1 + lado2 + lado3);
  END//
-- 04 - Procedimiento almacenado triangulos
CREATE OR REPLACE PROCEDURE actualizaTriangulos ( IN desde int, IN hasta int) -- CONTAINS SQL
  BEGIN
    UPDATE triangulos t
      SET area = areaTriangulo(t.base, t.altura), 
        perimetro = perimetroTriangulo(t.base, t.lado2, t.lado3) 
    WHERE t.id BETWEEN desde AND hasta;
  END//



-- 05 - Funci�n �rea cuadrado 
CREATE OR REPLACE FUNCTION areaCuadrado ( lado float ) RETURNS float
  BEGIN
    RETURN (lado * lado);
  END//
-- 06 - Funci�n perimetro cuadrado 
CREATE OR REPLACE FUNCTION perimetroCuadrado ( lado float ) RETURNS float
  BEGIN
    RETURN (lado * 4);
  END//
-- 07 - Procedimiento almacenado cuadrados
CREATE OR REPLACE PROCEDURE actualizaCuadrados ( IN desde int, IN hasta int) -- CONTAINS SQL
  BEGIN
    UPDATE cuadrados c
      SET area = areaCuadrado(c.lado), 
        perimetro = perimetroCuadrado(c.lado) 
    WHERE c.id BETWEEN desde AND hasta;
  END//


-- 08 - Funci�n �rea rect�ngulo 
CREATE OR REPLACE FUNCTION areaRectangulo ( base float, altura float ) RETURNS float
  BEGIN
    RETURN (base*altura);
  END//
-- 09 - Funci�n perimetro rect�ngulo 
CREATE OR REPLACE FUNCTION perimetroRectangulo ( base float, altura float ) RETURNS float
  BEGIN
    RETURN (2*base + 2*altura);
  END//
-- 10 - Procedimiento almacenado rect�ngulo
CREATE OR REPLACE PROCEDURE actualizaRectangulos ( IN desde int, IN hasta int) -- CONTAINS SQL
  BEGIN
    UPDATE rectangulo r
      SET area = areaRectangulo(r.lado1, r.lado2), 
        perimetro = perimetroRectangulo(r.lado1, r.lado2) 
    WHERE r.id BETWEEN desde AND hasta;
  END//



-- 11 - Funci�n �rea c�rculo
CREATE OR REPLACE FUNCTION areaCirculo ( radio float ) RETURNS float
  BEGIN
    RETURN (PI() * radio * radio); -- PI*r^2
  END//
-- 12 - Funci�n perimetro c�rculo 
CREATE OR REPLACE FUNCTION perimetroCirculo ( radio float ) RETURNS float
  BEGIN
    RETURN (2 * PI() * radio);
  END//
-- 13 - Procedimiento almacenado c�rculos
CREATE OR REPLACE PROCEDURE actualizaCirculos ( IN desde int, IN hasta int, IN tipo varchar(1)  ) -- CONTAINS SQL
  BEGIN
    IF tipo = NULL THEN
      UPDATE circulo c
        SET area = areaCirculo(radio), 
          perimetro = perimetroCirculo(radio)
      WHERE c.id BETWEEN desde AND hasta;
    ELSE 
      UPDATE circulo c
        SET area = areaCirculo(radio), 
          perimetro = perimetroCirculo(radio)
      WHERE c.id BETWEEN desde AND hasta AND c.tipo = tipo;
    END IF;
  END//


-- 14 - Funci�n media de 4 parametros
CREATE OR REPLACE FUNCTION media4 ( v1 float, v2 float, v3 float, v4 float ) RETURNS float
  BEGIN
    RETURN (v1+v2+v3+v4) / 4.0;
  END//
-- 15 - Funci�n m�nimo de 4 parametros
CREATE OR REPLACE FUNCTION min4 (  v1 float, v2 float, v3 float, v4 float ) RETURNS float
  BEGIN
    RETURN LEAST(v1,v2,v3,v4);
  END//
-- 16 - Funci�n m�ximo de 4 parametros
CREATE OR REPLACE FUNCTION max4 ( v1 float, v2 float, v3 float, v4 float ) RETURNS float
  BEGIN
    RETURN GREATEST(v1,v2,v3,v4);
  END//
-- 17 - Funci�n m�ximo de 4 parametros
CREATE OR REPLACE FUNCTION mode4 ( v1 float, v2 float, v3 float, v4 float ) RETURNS float
  BEGIN
    DECLARE modeValue float;
    CREATE OR REPLACE TEMPORARY TABLE scratch ( val float );
    INSERT INTO scratch (val) VALUES (v1),(v2),(v3),(v4);
    SELECT val INTO modeValue -- SELECT TOP 1 en otros sistemas
      FROM scratch 
      GROUP BY val 
      ORDER BY COUNT(*) DESC 
      LIMIT 0,1;
    RETURN modeValue;
  END//



-- 18 - Procedimiento almacenado alumnos
CREATE OR REPLACE PROCEDURE alumnos ( IN desde int, IN hasta int) -- CONTAINS SQL
  BEGIN
    DECLARE grupo int;
    DECLARE mediaG1, mediaG2 float;

    UPDATE alumnos a
      SET 
        a.media = media4(a.nota1, a.nota2, a.nota3, a.nota4),
        a.max = max4(a.nota1, a.nota2, a.nota3, a.nota4),
        a.min = min4(a.nota1, a.nota2, a.nota3, a.nota4),
        a.moda = mode4(a.nota1, a.nota2, a.nota3, a.nota4)
    WHERE a.id BETWEEN desde AND hasta;

    SELECT AVG(a.media) INTO mediaG1 FROM alumnos a
      WHERE (a.id BETWEEN desde AND hasta) AND (a.grupo = 1);
    SELECT AVG(a.media) INTO mediaG2 FROM alumnos a
      WHERE (a.id BETWEEN desde AND hasta) AND (a.grupo = 2);

    INSERT INTO grupos (id, media, max, min) VALUES (1, mediaG1, null, null) 
      ON DUPLICATE KEY UPDATE media = mediaG1;
    INSERT INTO grupos (id, media, max, min) VALUES (2, mediaG2, null, null) 
      ON DUPLICATE KEY UPDATE media = mediaG2;
  END//


-- 19 - Procedimiento almacenado conversion
CREATE OR REPLACE PROCEDURE conversion ( IN desde int ) -- CONTAINS SQL
  BEGIN
    DECLARE cm, m, km, pulgadas float;

    -- Actualizar los que tengan centimetros
    UPDATE conversion medida
      SET 
        medida.m = medida.cm / 100, 
        medida.km = medida.cm / 100000,
        medida.pulgadas = medida.cm/2.54 
    WHERE (medida.id >= desde) AND medida.cm IS NOT NULL;
    -- Actualizar los que tengan metros
    UPDATE conversion medida
      SET 
        medida.cm = 100*medida.m, 
        medida.km = medida.m / 1000, 
        medida.pulgadas = 100*medida.m/2.54 
    WHERE (medida.id >= desde) AND medida.m IS NOT NULL;
    -- Actualizar los que tengan kilometros
    UPDATE conversion medida
      SET 
        medida.cm = 100000*medida.km, 
        medida.m = 1000*medida.km, 
        medida.pulgadas = 100000*medida.km/2.54 
    WHERE (medida.id >= desde) AND medida.km IS NOT NULL;
    -- Actualizar los que tengan pulgadas
    UPDATE conversion medida
      SET 
        medida.cm = 2.54*medida.pulgadas, 
        medida.m = 2.54*medida.pulgadas/1000, 
        medida.km = 2.54*medida.pulgadas/100000 
    WHERE (medida.id >= desde) AND medida.pulgadas IS NOT NULL;
  END//

DELIMITER ;





SELECT areaTriangulo(1,1) AS areaTriangulo;
SELECT perimetroTriangulo(1,1,1) AS perimetroTriangulo;
CALL actualizaTriangulos(1,5);

SELECT areaCuadrado(3) AS areaCuadrado;
SELECT perimetroCuadrado(3) AS perimetroCuadrado;
CALL actualizaCuadrados(1,5);

SELECT areaRectangulo(3,5) AS areaRectangulo;
SELECT perimetroRectangulo(3,5) AS perimetroRectangulo;
CALL actualizaRectangulos(1,5);

SELECT areaCirculo(0.5) AS areaCirculo;
SELECT perimetroCirculo(0.5) AS perimetroCirculo;
CALL actualizaCirculos(1,5,'a');

SELECT
  media4(1,2,2,4) AS media,
  max4(1,2,2,4) AS maximo,
  min4(1,2,2,4) AS minimo,
  mode4(1,2,2,4) AS moda;

CALL alumnos(1,5);

CALL conversion(16);
SELECT * FROM conversion;
  ;
/*

Adem�s, el mismo procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que est�n comprendidos entre los id pasados y por grupo. 


CREATE OR REPLACE PROCEDURE conversion ( IN desde int ) -- CONTAINS SQL
  BEGIN
    
  END//


19.-  Procedimiento almacenado conversi�n 
Realizar un procedimiento almacenado que le pasas como argumento un id y te calcula la conversi�n de unidades de todos los registros que est�n detr�s de ese id. 
Si est� escrito los cm calculara m, km y pulgadas. Si est� escrito en m calculara cm, km y pulgadas y as� consecutivamente




*/