﻿USE programacion;

/**
 *  DISPARADORES
**/

-- Los disparadores actualizan los datos de un registro cuando se manipulan.
--
-- El disparador actua sobre el registro en el que se dispara, así que no 
-- puede manipularse con instrucciones de SQL, porque esta bloqueado por el 
-- propio disparador.
-- Es una funcion que manipula los datos del registro antes de manipularlo.



-- DISPARADOR
/*

DELIMITER //

CREATE [OR REPLACE] TRIGGER disparador

  AFTER INSERT -- BEFORE/AFTER INSERT/UPDATE/DELETE

  ON salas     -- Tabla que se referencia

  FOR EACH ROW
  
  BEGIN
        -- NEW se refiene al valor nuevo que se quiere dotar al registro  (si existe)
    SET NEW.total = @DELTA + OLD.total;
        -- OLD se refiere al valor que tenia el registro anteriormente (si existe)
  END//

DELIMITER ;
*/

/*
   INSERT INTO tabla (columns) VALUE (values)   (     NEW)
   UPDATE tabla SET values WHERE ID             (OLD, NEW)
   DELETE tabla WHERE ID                        (OLD     )

*/

/*
--
-- How to INSERT If Row Does Not Exist, if it does, IGNORE (INSERT IGNORE)
--
  MySQL:
    a) INSERT IGNORE
      Hace INSERT ó NADA.

--
-- How to INSERT If Row Does Not Exist, if it does, UPDATE (UPSERT)
--
  MySQL: 
    a) INSERT ... ON DUPLICATE KEY UPDATE SET=...;
      Hace INSERT ó UPDATE.
    b) REPLACE
      Hace DELETE+INSERT.

  SQLite:
    a) INSERT OR REPLACE INTO names (id, name) VALUES (1, "John") 
       ó (REPLACE is a alias of INSERT OR REPLACE)
       REPLACE INTO names (id, name) VALUES (1, "John")
      Hace DELETE+INSERT.

  PostgreSQL:
    a) Insert or update
    INSERT INTO the_table (id, name) VALUE (1, 'A') 
      ON CONFLICT (id) DO 
        UPDATE SET name = excluded.name;

Using 

  INSERT OR REPLACE INTO salas (id, butacas, fecha) VALUE (1, 25, "2015/01/01");
*/

/**
 * SNIPPET CreateTrigger
**/

CREATE OR REPLACE TABLE salas (
  id int AUTO_INCREMENT,
  butacas int,
  fecha date,
  edad int DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT,
  sala int,
  numero int DEFAULT 0,
  PRIMARY KEY (id)
);


DELIMITER //

CREATE OR REPLACE TRIGGER salasBI -- Salas BeforeInsert
  BEFORE INSERT 
  ON salas FOR EACH ROW
BEGIN
  SET new.edad = TIMESTAMPDIFF(year, new.fecha, NOW());
  /*
   * Equivale a hacer un "UPDATE salas s SET edad = TIMESTAMPDIFF(year, fecha, NOW());" 
   * pero más eficiente porque solo se ejecuta en los registros que se inserta.
   */
 END //

CREATE OR REPLACE TRIGGER salasBU -- Salas BeforeUpdate
  BEFORE UPDATE
  ON salas FOR EACH ROW
BEGIN
  SET new.edad = TIMESTAMPDIFF(year, new.fecha, NOW());
END;


DELIMITER ;



-- INSERT INTO salas (butacas, fecha) VALUE (50, "2010/01/01");          
-- UPDATE salas SET butacas = 25, fecha = "2015/01/01" WHERE id=1;
INSERT INTO salas (id, butacas, fecha) VALUE (1, 25, "2017/01/01") ON DUPLICATE KEY UPDATE butacas = 25, fecha="2017/01/01";
REPLACE INTO salas (id, butacas, fecha) VALUE (1, 28, "2013/01/01");

-- INSERT OR REPLACE INTO salas (id, butacas, fecha) VALUE (1, 25, "2015/01/01");

SELECT * FROM salas s;





/* 
-- Ejercicio
  Necesito que la tabla salas tenga 3 campos nuevos, dia, mes, año, con el dia, mes y año de la media.
  Quiero que esos 3 campos se actualicen automáticamente al actualizar la fecha mediante un disparador.
*/

ALTER TABLE salas ADD dia int DEFAULT 0, ADD mes int DEFAULT 0, ADD anno int DEFAULT 0;


DELIMITER //

CREATE OR REPLACE TRIGGER salasBI -- Salas BeforeInsert
  BEFORE INSERT 
  ON salas FOR EACH ROW
BEGIN
  SET new.edad = TIMESTAMPDIFF(year, new.fecha, NOW());
  SET new.dia = DAY(new.fecha);
  set new.mes = MONTH(new.fecha);
  set new.anno = YEAR(new.fecha);
 END //

CREATE OR REPLACE TRIGGER salasBU -- Salas BeforeUpdate
  BEFORE UPDATE
  ON salas FOR EACH ROW
BEGIN
  SET new.edad = TIMESTAMPDIFF(year, new.fecha, NOW());
  SET new.dia = DAY(new.fecha);
  set new.mes = MONTH(new.fecha);
  set new.anno = YEAR(new.fecha);
END //

DELIMITER ;

UPDATE salas set fecha=fecha; 








-- Ejercicio
CREATE OR REPLACE TABLE ventasUno (
  id int AUTO_INCREMENT,
  fecha date,
  punitario decimal(10,2) DEFAULT 0,
  unidades int DEFAULT 0,
  pfinal decimal(10,2) DEFAULT 0,
  PRIMARY KEY (id)
);


DELIMITER //

CREATE OR REPLACE TRIGGER ventasUnoBI
  BEFORE INSERT ON ventasUno FOR EACH ROW
BEGIN
  SET new.pfinal = new.unidades * new.punitario;
END //

CREATE OR REPLACE TRIGGER ventasUnoBU
  BEFORE UPDATE ON ventasUno FOR EACH ROW
BEGIN
  SET new.pfinal = new.unidades * new.punitario;
END //

DELIMITER ;

INSERT INTO ventasUno (fecha, punitario, unidades)
  VALUES (CURDATE(), 13.50, 2);

SELECT * FROM ventasUno;

UPDATE ventasUno SET pfinal = pfinal WHERE true;