/*
ejemplos 4 procedimientos
ejemplos 5 funciones
ejemplos 6 disparadores


*/



CREATE DATABASE IF NOT EXISTS ejemploTriggers 
  DEFAULT CHARACTER SET utf8mb4 
  DEFAULT COLLATE utf8mb4_spanish_ci;

USE ejemploTriggers;




 
-- Borra todas las tablas

DELETE FROM usuario WHERE TRUE;
DROP table rNNUsuarioUsaLocal;
DROP TRIGGER usuarioBI;
DROP TRIGGER usuarioBU;
DROP table usuario;
DROP table local;
SHOW FULL TABLES;













CREATE OR REPLACE TABLE local (
  id int AUTO_INCREMENT,
  nombre varchar(50),
  precio float,
  plazas int,
  PRIMARY KEY (id)
) ENGINE=INNODB;


CREATE OR REPLACE TABLE usuario (
  id int AUTO_INCREMENT,
  nombre varchar(50),
  inicial char(1),
  PRIMARY KEY (id)
) ENGINE=INNODB;

/* Modificar la tabla usuario */
ALTER TABLE usuario
  ADD COLUMN IF NOT EXISTS contador int DEFAULT 0;


CREATE OR REPLACE TABLE rNNUsuarioUsaLocal (
  idUsuario int,
  idLocal int,
  fecha date,
  dias int,
  total float,
  PRIMARY KEY (idUsuario, idLocal),
  CONSTRAINT fk_idUsuario FOREIGN KEY (idUsuario) REFERENCES usuario(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_idLocal FOREIGN KEY (idLocal) REFERENCES local(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;


DELIMITER //

CREATE OR REPLACE TRIGGER usuarioBI 
  BEFORE INSERT ON usuario FOR EACH ROW
BEGIN
  SET new.inicial = LEFT(new.nombre, 1);
END //

CREATE OR REPLACE TRIGGER usuarioBU 
  BEFORE INSERT ON usuario FOR EACH ROW
BEGIN
  SET new.inicial = LEFT(new.nombre, 1);
END //

CREATE OR REPLACE TRIGGER rNNUsuarioUsaLocalAI 
  AFTER INSERT ON rNNUsuarioUsaLocal FOR EACH ROW
BEGIN
  UPDATE usuario u 
    SET u.contador = u.contador + 1 
    WHERE u.id = new.idUsuario;
END //



DELIMITER ;



SHOW TABLES;

/* Insertar un usuario */
INSERT INTO usuario (nombre) VALUES
  ("Roberto"), ("Ana"), ("Kevin");
INSERT INTO local (id, nombre, precio, plazas) VALUES 
  (1, "Albergue El Campero Gallino", 26, 52),
  (2, "Pensi�n La Loli", 38, 6),
  (3, "Puertochico", 8, 24);
INSERT INTO rNNUsuarioUsaLocal (idUsuario, idLocal, fecha, dias) VALUES 
  (1, 1, CURDATE(), 2),
  (1, 2, FROM_DAYS(TO_DAYS(CURDATE())+2), 5),
  (2, 2, FROM_DAYS(TO_DAYS(CURDATE())-1), 3),
  (2, 1, FROM_DAYS(TO_DAYS(CURDATE())+8), 2),
  (3, 3, CURDATE(), 1);

SELECT * FROM local l;
SELECT * FROM usuario u;
SELECT * FROM rNNUsuarioUsaLocal nul;

SELECT nul.idUsuario, COUNT(*) contador FROM rNNUsuarioUsaLocal nul GROUP BY nul.idUsuario;

SELECT * FROM usuario u INNER JOIN (SELECT nul.idUsuario, COUNT(*) contador FROM rNNUsuarioUsaLocal nul GROUP BY nul.idUsuario) t ON (u.id = t.idUsuario);

UPDATE usuario u SET contador = 0;

UPDATE usuario u INNER JOIN (SELECT nul.idUsuario, COUNT(*) contador FROM rNNUsuarioUsaLocal nul GROUP BY nul.idUsuario) t ON (u.id = t.idUsuario)
  SET u.contador = t.contador
  WHERE true;

/*MAL: no puedes relacionar ambos id de usuario, el del calculo y el del update, en el mismo paso. USAR JOIN
  UPDATE usuario u
  SET u.contador = (SELECT COUNT(*) FROM rNNUsuarioUsaLocal nul WHERE nu )
  WHERE true;

*/